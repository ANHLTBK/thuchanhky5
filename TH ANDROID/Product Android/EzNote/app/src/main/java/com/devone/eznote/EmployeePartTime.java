package com.devone.eznote;

public class EmployeePartTime extends Employee {
    @Override
    public double TinhLuong(){
        return 150;
    }
    public String toString(){
        return super.toString()+" ; Luong = " + TinhLuong();
    }
}
