package com.devone.eznote;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

public class Browser extends AppCompatActivity {

    ImageButton imgGG, imgFB, imgYT, imgMp3, imgPic,btnFind, imgnext, imgback, imgreload;
    EditText editURL;
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browser);
        inDeclare();

    }

    private void inDeclare() {
        imgGG = findViewById(R.id.imgGG);
        imgFB = findViewById(R.id.imgFB);
        imgYT = findViewById(R.id.imgYT);
        imgMp3 = findViewById(R.id.imgMp3);
        imgPic = findViewById(R.id.imgPic);
        btnFind = findViewById(R.id.imgFind);
        editURL = findViewById(R.id.editURL);
        imgback = findViewById(R.id.imgBack);
        imgnext = findViewById(R.id.imgNext);


    }
}
