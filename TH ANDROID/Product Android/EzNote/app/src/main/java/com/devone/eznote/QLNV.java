package com.devone.eznote;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class QLNV extends AppCompatActivity {
    EditText manv, tennv;
    Button them;
    RadioGroup radgroup;
    ListView listnv;
    ArrayList<Employee> arrEmployee=new ArrayList<Employee>();
    ArrayAdapter<Employee> adapter=null;
    Employee employee=null;
    int selectedItems=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qlnv);
        inDeclare();
        inEvent();
    }

    private void inEvent() {
        them.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View agr0) {
                progessNhap();
            }
        });
        listnv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> agr0, View agr1, int agr2, long agr3) {
                selectedItems = agr2;
                showAlertDialog();
                return false;
            }
        });
        listnv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Employee temp = arrEmployee.get(i);
                manv.setText(temp.getId());
                tennv.setText(temp.getName());
                if (temp instanceof EmployeeFullTime){
                    RadioButton r = (RadioButton) findViewById(R.id.radChinhthuc);
                    r.setChecked(true);
                }
                if (temp instanceof EmployeePartTime){
                    RadioButton r = (RadioButton) findViewById(R.id.radThoivu);
                    r.setChecked(true);
                }
            }
        });
    }

    private void progessNhap() {
        int radID = radgroup.getCheckedRadioButtonId();
        String id = manv.getText()+"";
        String name = tennv.getText()+"";
        if (radID == R.id.radChinhthuc){
            employee = new EmployeeFullTime();
        } else {
            employee = new EmployeePartTime();
        }
        employee.setID(id);
        employee.setName(name);
        arrEmployee.add(employee);
        adapter.notifyDataSetChanged();
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Xac nhan xoa du lieu");
        builder.setMessage("Ban co muon xoa nhan vien nay?");
        builder.setCancelable(false);
        builder.setNegativeButton("Khong", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(QLNV.this,"Khong xoa duoc",Toast.LENGTH_LONG).show();
            }
        });
        builder.setPositiveButton("Co", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                arrEmployee.remove(selectedItems);
                adapter.notifyDataSetChanged();
                Toast.makeText(QLNV.this,"Da xoa",Toast.LENGTH_LONG).show();
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void inDeclare() {
        manv = findViewById(R.id.editMaNV);
        tennv = findViewById(R.id.editTenNV);
        them = findViewById(R.id.btnThemNV);
        radgroup = findViewById(R.id.radiogr);
        listnv = findViewById(R.id.ListNV);
        adapter = new ArrayAdapter<Employee>(this, android.R.layout.simple_list_item_1,arrEmployee);
        listnv.setAdapter(adapter);
    }
}
