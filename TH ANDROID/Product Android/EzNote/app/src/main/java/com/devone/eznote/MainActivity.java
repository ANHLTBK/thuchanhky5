package com.devone.eznote;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {
    Button add;
    EditText nhapnd;
    ListView list;
    ArrayList<String> notes;
    ArrayAdapter<String> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inEvent();
        inExcute();

    }

    private void inExcute() {
        nhapnd.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN){
                    String note = nhapnd.getText().toString();
                    notes.add(note);
                    adapter.notifyDataSetChanged();
                }
                return false;
            }
        });
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            String content = nhapnd.getText().toString();
            notes.add(content);
            adapter.notifyDataSetChanged();
            nhapnd.setText("");
            }
        });
    }

    private void inEvent() {
        add = findViewById(R.id.btnAdd);
        nhapnd = findViewById(R.id.editNoidung);
        list = findViewById(R.id.lsView);
        notes = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.myarray)));
        //notes.add("Hoc thuc hanh Android");
        //notes.add("Hoc thuc hanh Android");
        //notes.add("Hoc thuc hanh Android");
        adapter = new ArrayAdapter<String>(this, R.layout.items_notes,notes);
        list.setAdapter(adapter);
    }

}
