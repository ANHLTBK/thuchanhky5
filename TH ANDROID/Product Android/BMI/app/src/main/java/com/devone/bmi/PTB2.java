package com.devone.bmi;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PTB2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ptb2);
        final EditText nhapa = findViewById(R.id.edita);
        final EditText nhapb = findViewById(R.id.editb);
        final EditText nhapc = findViewById(R.id.editc);
        final TextView ketqua = findViewById(R.id.txtKQ);
        Button Tinh = findViewById(R.id.btnGPT);
        Tinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double a = Double.parseDouble(nhapa.getText()+"");
                double b = Double.parseDouble(nhapb.getText()+"");
                double c = Double.parseDouble((nhapc.getText()+""));
                double x = -b/a;
                double delta = (b*b-4*a*c);
                double x1 = (-b + Math.sqrt(delta)/2*a);
                double x2 = (-b - Math.sqrt(delta)/2*a);
                double xx = -b/2*a;

                if(a==0){
                    ketqua.setText("Khong phai PTB2");
                } else {
                    if(delta<0){
                        ketqua.setText("PT vo nghiem");
                    }else  if(delta == 0){
                        ketqua.setText(""+x);
                    } else {
                        ketqua.setText("X1 = " + x1 + "X2 = "+x2);
                    }
                }
            }
        });
    }
}
