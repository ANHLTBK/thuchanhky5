package com.ltmt1.intentdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        TextView tvThongbao2 = (TextView) findViewById(R.id.tvThongBao2);

        //Lấy intent gọi activity này
        Intent caller = getIntent();
        //Lấy bundle dựa vào key
        Bundle bundleFromCaller = caller.getBundleExtra("heso");
        //Lấy các giá trị trong bundle dựa vào key
        int a = bundleFromCaller.getInt("a");
        int b = bundleFromCaller.getInt("b");
        String kq = "";
        if(a==0 && b==0){
            kq = "Vô số nghiệm";
        }
        else if(a==0 && b!=0){
            kq = "Vô nghiệm";
        }
        else{
            DecimalFormat dcf = new DecimalFormat("0.##");
            kq = dcf.format(-b*1.0/a);
        }
        tvThongbao2.setText(kq);

        Button btnBack2 = (Button) findViewById(R.id.btnBack2);
        btnBack2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(ResultActivity.this, MainActivity.class);
                startActivity(myIntent);
            }
        });
    }
}
