package com.ltmt1.intentdemo;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnOpen = (Button) findViewById(R.id.btnOpen);
        btnOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(MainActivity.this, ChildActivity.class);
                startActivity(myIntent);
            }
        });

        final EditText edita = (EditText) findViewById(R.id.edita);
        final EditText editb = (EditText) findViewById(R.id.editb);
        Button btnResult = (Button) findViewById(R.id.btnResult);
        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Tạo intent để mở ResultActivity
                Intent myIntent = new Intent(MainActivity.this, ResultActivity.class);
                //Khai báo bundle chứa dữ liệu chuyển đi
                Bundle myBundle = new Bundle();
                //Đưa dữ liệu vào bundle
                int a = Integer.parseInt(edita.getText().toString());
                int b = Integer.parseInt(editb.getText().toString());
                myBundle.putInt("a", a);
                myBundle.putInt("b", b);
                //Đưa bundle vào Intent
                myIntent.putExtra("heso",myBundle);
                //Mở Activity kết quả
                startActivity(myIntent);
            }
        });

    }
}
