package com.ltmt1.qlnhnvin;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText editMaNV,editTenNV;
    Button btnThemNV;
    RadioGroup radgroup;
    ListView listNV;
    ArrayList<Employee> arrEmployee=new ArrayList<Employee>();
    ArrayAdapter<Employee> adapter=null;

    //Khai báo 1 employee object
    Employee employee=null;
    //Khai báo phần tử được chọn trong dánh sách
    int selectedItem = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //1. Lấy các đối tượng View dựa vào ID
        editMaNV = (EditText) findViewById(R.id.editMaNV);
        editTenNV = (EditText) findViewById(R.id.editTenNV);
        btnThemNV = (Button) findViewById(R.id.btnThemNV);
        radgroup = (RadioGroup) findViewById(R.id.radgroup);
        listNV = (ListView) findViewById(R.id.listNV);
        //2. Gắn Data Source là các employee vào Adapter
        adapter=new ArrayAdapter<Employee>(this,
                android.R.layout.simple_list_item_1,
                arrEmployee);
        //3. Gắn Adapter vào ListView
        listNV.setAdapter(adapter);

        btnThemNV.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                processNhap();
            }
        });

        //Xử lý sự kiện Long click
        listNV.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                selectedItem = arg2;
                showAlertDialog();
                return false;
            }
        });
        //Xử lý sự kiện khi chọn một mục trong danh sách sẽ hiển thị thông tin trong
        //các điều khiển phía trên
        listNV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Employee temp = arrEmployee.get(i);
                editMaNV.setText(temp.getId());
                editTenNV.setText(temp.getName());
                if (temp instanceof EmployeeFullTime){
                    RadioButton r = (RadioButton) findViewById(R.id.radChinhThuc);
                    r.setChecked(true);
                }
                if (temp instanceof EmployeePartTime){
                    RadioButton r = (RadioButton) findViewById(R.id.radThoiVu);
                    r.setChecked(true);
                }
            }
        });
    }

    private void showAlertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Xác nhận xoá dữ liệu");
        builder.setMessage("Bạn có muốn xoá nhân viên này không?");
        builder.setCancelable(false);
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogInterface, int i) {
                Toast.makeText(MainActivity.this, "Không xoá được",
                        Toast.LENGTH_SHORT).show();
            }
        });
        builder.setPositiveButton("Có", new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogInterface, int i) {
                arrEmployee.remove(selectedItem);//xóa phần tử được chọn
                adapter.notifyDataSetChanged();
                Toast.makeText(MainActivity.this, "Đã xoá dữ liệu",
                        Toast.LENGTH_SHORT).show();
                dialogInterface.dismiss();

            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void processNhap() {
        //Lấy ra đúng id của Radio Button được checked
        int radId = radgroup.getCheckedRadioButtonId();
        String id = editMaNV.getText()+"";
        String name = editTenNV.getText()+"";
        if(radId == R.id.radChinhThuc)        {
            //tạo đối tượng là FullTime
            employee=new EmployeeFullTime();
        }else{
            //Tạo đối tượng là Partime
            employee=new EmployeePartTime();
        }
        //Thiết lập dữ liệu cho đối tượng
        employee.setId(id);
        employee.setName(name);
        //Đưa employee vào ArrayList
        arrEmployee.add(employee);
        //Cập nhập giao diện
        adapter.notifyDataSetChanged();
    }
}
