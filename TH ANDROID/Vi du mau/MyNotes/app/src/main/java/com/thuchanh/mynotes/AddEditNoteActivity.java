package com.thuchanh.mynotes;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.thuchanh.helper.MyDBHelper;
import com.thuchanh.model.Note;

public class AddEditNoteActivity extends AppCompatActivity {

    //Khai báo các biến và hằng
    Note note;
    //Giá trị hằng xác định màn hình này được mở
    //cho việc tạo mới hay chỉnh sửa ghi chú
    private static final int MODE_CREATE = 1;
    private static final int MODE_EDIT = 2;

    private int mode;
    private EditText txtNodeTitle, txtNodeContent;
    private Button btnSave, btnCancel;

    private boolean needRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_note);
        //Lấy về các điều khiển qua ID
        txtNodeTitle = (EditText) findViewById(R.id.txtNoteTitle);
        txtNodeContent = (EditText) findViewById(R.id.txtNoteContent);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        //Lấy về đối tượng Note được gửi từ MainActivity qua Intent
        Intent intent = this.getIntent();
        this.note = (Note) intent.getSerializableExtra("note");
        if(note == null)  {
            this.mode = MODE_CREATE;
        } else {
            this.mode = MODE_EDIT;
            this.txtNodeTitle.setText(note.getNoteTitle());
            this.txtNodeContent.setText(note.getNoteContent());
        }
        //Xử lý sự kiện nhấn vào nút Save
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveNote();
            }
        });
        //Xử lý sự kiện nhấn vào nút Cancel
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Không làm gì, trở về MainActivity.
                finish();
            }
        });
    }
    //Phương thức xử lý lưu ghi chú
    private void saveNote(){
        MyDBHelper db = new MyDBHelper(this);

        String title = this.txtNodeTitle.getText().toString();
        String content = this.txtNodeContent.getText().toString();

        if(title.equals("") || content.equals("")) {
            Toast.makeText(getApplicationContext(),
                    "Hãy nhập tiêu đề & nội dung",
                    Toast.LENGTH_LONG).show();
            return;
        }
        if(mode==MODE_CREATE ) {
            this.note= new Note(title,content);
            db.addNote(note);
        } else  {
            this.note.setNoteTitle(title);
            this.note.setNoteContent(content);
            db.updateNote(note);
        }
        this.needRefresh = true;
        // Trở lại MainActivity.
        this.onBackPressed();
    }

    //Khi Activity này hoàn thành,
    //có thể cần gửi phản hồi gì đó về cho Activity đã gọi nó.
    @Override
    public void finish() {
        // Chuẩn bị dữ liệu Intent.
        Intent data = new Intent();
        // Yêu cầu MainActivity refresh lại ListView hoặc không.
        data.putExtra("needRefresh", needRefresh);
        // Activity đã hoàn thành OK, trả về dữ liệu.
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }
}
