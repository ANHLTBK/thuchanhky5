package com.thuchanh.mynotes;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.thuchanh.helper.MyDBHelper;
import com.thuchanh.model.Note;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //Khai báo các biến
    ListView lvNotes;
    //Khai báo các giá trị hằng là các mục menu
    private static final int MENU_ITEM_VIEW = 111;
    private static final int MENU_ITEM_EDIT = 222;
    private static final int MENU_ITEM_CREATE = 333;
    private static final int MENU_ITEM_DELETE = 444;
    private static final int MY_REQUEST_CODE = 1000;

    //Khai báo ArrayList và ArrayAdapter cho ListView
    private final ArrayList<Note> noteList = new ArrayList<Note>();
    private ArrayAdapter<Note> listViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Lấy về các điều khiển qua ID
        lvNotes = (ListView) findViewById(R.id.lvNotes);

        //Tạo CSDL, khởi tạo dữ liệu mẫu và gán vào ListView
        MyDBHelper db = new MyDBHelper(this);
        db.createDefaultNotesIfNeed();

        noteList.addAll(db.getAllNotes());
        // Định nghĩa một Adapter gồm các tham số
        // 1 - Context
        // 2 - Layout cho các dòng
        // 3 - Danh sách dữ liệu
        listViewAdapter = new ArrayAdapter<Note>(this,
                android.R.layout.simple_list_item_1, noteList);
        // Đăng ký Adapter cho ListView.
        lvNotes.setAdapter(listViewAdapter);

        // Đăng ký Context menu cho ListView.
        registerForContextMenu(lvNotes);
    }

    //Tạo menu ngữ cảnh (context menu)
    //Trong bài này menu được tạo bằng code
    //và hiển thị mặc định không tạo layout XML
    @Override
    public void onCreateContextMenu(ContextMenu menu, View view,
                                    ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, view, menuInfo);
        menu.setHeaderTitle("Chọn hành động");
        // Các tham số: groupId, itemId, order, title
        menu.add(0, MENU_ITEM_VIEW , 0, "Xem ghi chú");
        menu.add(0, MENU_ITEM_CREATE , 1, "Tạo ghi chú");
        menu.add(0, MENU_ITEM_EDIT , 2, "Sửa ghi chú");
        menu.add(0, MENU_ITEM_DELETE, 4, "Xoá ghi chú");
    }
    //Phương thức xử lý sự kiện khi chọn một mục trong menu
    @Override
    public boolean onContextItemSelected(MenuItem item){
        AdapterView.AdapterContextMenuInfo
                info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        final Note selectedNote = (Note) lvNotes.getItemAtPosition(info.position);

        if(item.getItemId() == MENU_ITEM_VIEW){
            Toast.makeText(getApplicationContext(),
                    selectedNote.getNoteContent(),Toast.LENGTH_LONG).show();
        }
        else if(item.getItemId() == MENU_ITEM_CREATE){
            Intent intent = new Intent(this, AddEditNoteActivity.class);
            //Mở giao diện AddEditNoteActivity để thêm mới, có phản hồi
            this.startActivityForResult(intent, MY_REQUEST_CODE);
        }
        else if(item.getItemId() == MENU_ITEM_EDIT ){
            Intent intent = new Intent(this, AddEditNoteActivity.class);
            intent.putExtra("note", selectedNote);
            //Mở giao diện AddEditNoteActivity để sửa, có phản hồi
            //Gửi kèm theo ghi chú đang được chọn trong danh sách
            this.startActivityForResult(intent,MY_REQUEST_CODE);
        }
        else if(item.getItemId() == MENU_ITEM_DELETE){
            // Hỏi trước khi xóa
            new AlertDialog.Builder(this)
                    .setMessage(selectedNote.getNoteTitle()+
                            ". Bạn có chắc chắn muốn xoá?")
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            deleteNote(selectedNote);
                        }
                    })
                    .setNegativeButton("Không", null)
                    .show();
        }
        else {
            return false;
        }
        return true;
    }
    // Người dùng đồng ý xóa một Note.
    private void deleteNote(Note note)  {
        MyDBHelper db = new MyDBHelper(this);
        db.deleteNote(note);
        this.noteList.remove(note);
        // Refresh ListView.
        this.listViewAdapter.notifyDataSetChanged();
    }
    // Khi AddEditNoteActivity hoàn thành, nó gửi phản hồi lại
    @Override
    protected void onActivityResult(int requestCode,
                                    int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK &&
                requestCode == MY_REQUEST_CODE ) {
            boolean needRefresh =
                    data.getBooleanExtra("needRefresh",true);
            // Refresh ListView
            if(needRefresh) {
                this.noteList.clear();
                MyDBHelper db = new MyDBHelper(this);
                List<Note> list=  db.getAllNotes();
                this.noteList.addAll(list);
                // Thông báo dữ liệu thay đổi (Để refresh ListView).
                this.listViewAdapter.notifyDataSetChanged();
            }
        }
    }
}
