package com.ltmt2.kiemtraandroid.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.ltmt2.kiemtraandroid.R;
import com.ltmt2.kiemtraandroid.model.MonAn;

import java.util.ArrayList;

public class MonAnAdapter extends ArrayAdapter<MonAn> {
    Activity context;
    int layoutId;

    ArrayList<MonAn> arrMonAn;
    public MonAnAdapter(Activity context, int textViewResourceId, ArrayList<MonAn> objects){
        super(context, textViewResourceId, objects);
        this.context = context;
        this.layoutId = textViewResourceId;
        this.arrMonAn = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = context.getLayoutInflater().inflate(layoutId, null);

        TextView txtTenMon = (TextView) convertView.findViewById(R.id.txtTenMon);
        TextView txtGia = (TextView) convertView.findViewById(R.id.txtGiaTien);
        ImageView imgview = (ImageView)convertView.findViewById(R.id.imgview);

        MonAn mon = arrMonAn.get(position);
        txtTenMon.setText(mon.toString());

        String stringGia = "Giá tiền: " + mon.getGiaTien();

        imgview.setImageResource(R.drawable.monb);
        if(mon.getLoaiMon().equals("ChauAu")){
            imgview.setImageResource(R.drawable.monanb);
        }

        txtGia.setText(stringGia);
        return convertView;
    }
}
