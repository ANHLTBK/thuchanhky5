package com.ltmt2.kiemtraandroid;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.ltmt2.kiemtraandroid.adapter.MonAnAdapter;
import com.ltmt2.kiemtraandroid.helper.DBHelper;
import com.ltmt2.kiemtraandroid.model.MonAn;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final int MENU_ITEM_ADD = 111;
    private static final int MENU_ITEM_EDIT = 444;
    private static final int MENU_ITEM_VIEW = 333;
    private static final int MENU_ITEM_DELETE = 222;
    private static final int MY_REQUEST_CODE = 100;
    DBHelper db = new DBHelper(this);
    ListView lvMonan;
    private ArrayList<MonAn> listMonAn = new ArrayList<MonAn>();
    private MonAnAdapter adapterMonAn = null;

    private MonAn monanSelected = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//Lấy về các điều khiển qua ID
        lvMonan = (ListView) findViewById(R.id.lvMonAn);
//Tạo CSDL, khởi tạo dữ liệu mẫu và gán vào ListView
        DBHelper db = new DBHelper(this);
        db.createDefaultNotesIfNeed();

        listMonAn.addAll(db.getAllMonAn());
        // đẩy dl vào Adapter
        // Định nghĩa một Adapter gồm các tham số
        // 1 - Context
        // 2 - Layout cho các dòng
        // 3 - Danh sách dữ liệu

        adapterMonAn = new MonAnAdapter(this, R.layout.listview_custom, listMonAn);
        // Đăng ký Adapter cho ListView.

        lvMonan.setAdapter(adapterMonAn);
        // Đăng ký Context menu cho ListView.
        registerForContextMenu(lvMonan);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Chọn hành động");
        //Các tham số: groupId, itemID, order, title

        menu.add(0, MENU_ITEM_ADD , 0, "Thêm Món Ăn");
        menu.add(0,MENU_ITEM_DELETE, 1, "Xóa");
        menu.add(0, MENU_ITEM_VIEW , 2, "Xem Danh Sách Món Ăn");
        menu.add(0, MENU_ITEM_EDIT , 3, "Sửa");
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final MonAn selectMonAn =(MonAn) lvMonan.getItemAtPosition(info.position);
        if(item.getItemId() == MENU_ITEM_VIEW){
            // Toast là kiểu hiển thị nên màn hình trong thời gian ngắn
            Toast.makeText(getApplicationContext(),
                    selectMonAn.getTenMon(),Toast.LENGTH_LONG).show();
        }
        else if(item.getItemId() == MENU_ITEM_ADD){
            // Intent chuyền dữ liệu dauwx các activity
            Intent intent = new Intent(this, AddMonAnActivity.class);
            //Mở giao diện AddMonAnActivity để thêm mới, có phản hồi
            this.startActivityForResult(intent, MY_REQUEST_CODE);
        }
        else if(item.getItemId() == MENU_ITEM_EDIT ){
            // Intent chuyền dữ liệu dauwx các activity
            Intent intent = new Intent(this, AddMonAnActivity.class);
            intent.putExtra("note", selectMonAn);
            //Mở giao diện AddMonAnActivity để sửa, có phản hồi
            //Gửi kèm theo ghi chú đang được chọn trong danh sách
            this.startActivityForResult(intent,MY_REQUEST_CODE);
        }
       if(item.getItemId() == MENU_ITEM_DELETE)
        {
            //Hỏi trước khi xóa
            new AlertDialog.Builder(this)
                    .setTitle("Xác nhận xóa dữ liệu")
                    .setMessage("Bạn có chắc chắn muốn xóa " + selectMonAn.getTenMon() + " ?")
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteNote(selectMonAn);
                        }
                    })
                    .setNegativeButton("Không", null)
                    .show();
        }
        else {
            return false;
        }
        return true;
    }

    public void deleteNote(MonAn mon){
        DBHelper db = new DBHelper(this);
        db.deleteNote(mon);
        this.listMonAn.remove(mon);
        // Refresh Listview
        this.adapterMonAn.notifyDataSetChanged();
    }
    // Khi AddEditNoteActivity hoàn thành, nó gửi phản hồi lại
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == MY_REQUEST_CODE) {
            reloadSinhVien();
        }
    }
    private void reloadSinhVien() {
        listMonAn.clear();
        ArrayList<MonAn> list = db.getAllMonAn();
        listMonAn.addAll(list);
        this.adapterMonAn.notifyDataSetChanged();
    }

}
