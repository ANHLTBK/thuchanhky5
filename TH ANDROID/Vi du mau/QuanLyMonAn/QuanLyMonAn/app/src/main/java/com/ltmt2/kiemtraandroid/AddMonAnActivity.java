package com.ltmt2.kiemtraandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ltmt2.kiemtraandroid.helper.DBHelper;
import com.ltmt2.kiemtraandroid.model.MonAn;

public class AddMonAnActivity extends AppCompatActivity {
    MonAn monan;

    private EditText edtTen, edtGia;
    private RadioGroup rgLoai;
    private RadioButton rChauA, rChauAu;
    private Button btnXoa, btnLuu;
    private boolean needRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_mon_an);
        
        edtTen = (EditText) findViewById(R.id.edtTenMon);
        edtGia = (EditText) findViewById(R.id.edtGiaTien);
        rChauA = (RadioButton) findViewById(R.id.rChauA);
        rChauAu = (RadioButton) findViewById(R.id.rChauAu);

        btnXoa = (Button) findViewById(R.id.btnXoa);
        btnLuu = (Button) findViewById(R.id.btnLuu);

        btnXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtTen.setText("");
                edtGia.setText("");
            }
        });

        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveMonAn();
            }
        });


    }
   // them Mon an
    private void saveMonAn() {
        DBHelper db = new DBHelper(this);
        String ten = this.edtTen.getText().toString();
        String gia = this.edtGia.getText().toString();

        if(ten.equals("") || gia.equals("")){
            Toast.makeText(getApplicationContext(), "Hãy nhập nội dung", Toast.LENGTH_SHORT).show();
            return;
        }
        else {

            String loai = "ChauA";
            if(rChauA.isChecked()){
                loai = "ChauAu";
            }
            this.monan = new MonAn(ten, Integer.parseInt(gia), loai);
            db.addNote(monan);
        }
        this.needRefresh = true;
        // tro lai MainAc..
        this.onBackPressed();
    }

    @Override
    public void finish() {
        //Chuan bi du lieu Intent
        Intent data = new Intent();
        //Yeu cau Main refresh lai ListView hoac khong
        data.putExtra("needRefresh", needRefresh);
        // Activity da hoan thanh OK, tra ve du lieu
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }
}
