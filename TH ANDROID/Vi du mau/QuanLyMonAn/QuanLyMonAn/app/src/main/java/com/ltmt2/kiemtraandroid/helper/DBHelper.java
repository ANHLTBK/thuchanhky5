package com.ltmt2.kiemtraandroid.helper;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ltmt2.kiemtraandroid.model.MonAn;

import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "QLMA";
    // Table Name: Note.
    private static final String TABLE_MONAN = "monan";
    private static final String COLUMN_MONAN_MA = "ma_monan";
    private static final String COLUMN_MONAN_TEN = "ten_monan";
    private static final String COLUMN_MONAN_GIA = "gia_tien";
    private static final String COLUMN_MONAN_LOAI = "loai_monan";

    public DBHelper(Context context){
        super(context, DATABASE_NAME, null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String script = "CREATE TABLE " + TABLE_MONAN + "("
                + COLUMN_MONAN_MA + " INTEGER PRIMARY KEY,"
                + COLUMN_MONAN_TEN + " TEXT,"
                + COLUMN_MONAN_GIA + " INTEGER,"
                + COLUMN_MONAN_LOAI + " TEXT" + ")";

        db.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MONAN);
        // Va tao lai
        onCreate(db);
    }

    // Nếu trong bảng Note chưa có dữ liệu,
    // thêm vào mặc đinhj 2 bản ghi
    public void createDefaultNotesIfNeed(){
        int count = this.getMonAnCount();
        if(count == 0){
            MonAn mon1 = new MonAn(1, "Bún bò Hầm Nghệ An", 6000, "ChauA");
            MonAn mon2 = new MonAn(2, "Bò Úc hầm rượu vang", 80000, "ChauAu");

            this.addNote(mon1);
            this.addNote(mon2);
        }
    }

    public int getMonAnCount(){
        String countQuery = "SELECT * FROM " + TABLE_MONAN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void addNote(MonAn mon){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_MONAN_TEN, mon.getTenMon());
        values.put(COLUMN_MONAN_GIA, mon.getGiaTien());
        values.put(COLUMN_MONAN_LOAI, mon.getLoaiMon());

        // Chèn 1 dòng dữ liệu vào bảng
        db.insert(TABLE_MONAN, null,values);
        //Close connect
        db.close();
    }

    public MonAn getMonAn(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_MONAN, new String[]{COLUMN_MONAN_TEN,
                        COLUMN_MONAN_GIA,
                        COLUMN_MONAN_LOAI},
                COLUMN_MONAN_MA + "=?",
                new String[]{String.valueOf(id)},
                null, null, null,null);
        if(cursor != null)
            cursor.moveToFirst(); // Di chuyển con trỏ lên đầu bảng

        //MonAn mon = new MonAn(Integer.parseInt(cursor.getString(0)),
          //      cursor.getString(1), cursor.getString(2));

        MonAn mon = new MonAn(cursor.getString(0),
        Integer.parseInt(cursor.getString(1)), cursor.getString(2));
        //return note
        return mon;
    }

    public ArrayList<MonAn> getAllMonAn(){
        ArrayList<MonAn> noteList = new ArrayList<MonAn>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_MONAN;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        // Duyệt trên con trỏ, và thêm vào danh sách
        if(cursor.moveToFirst()){
            do{
                MonAn note = new MonAn();
                note.setMaMon(Integer.parseInt(cursor.getString(0)));
                note.setTenMon(cursor.getString(1));
                note.setGiaTien(Integer.parseInt(cursor.getString(2)));
                note.setLoaiMon(cursor.getString(3));

                //Thêm vào danh sách
                noteList.add(note);
            }while (cursor.moveToNext());
        }
        return noteList;
    }

    public int updateNote(MonAn mon){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_MONAN_TEN, mon.getTenMon());
        values.put(COLUMN_MONAN_GIA, mon.getGiaTien());
        values.put(COLUMN_MONAN_LOAI, mon.getLoaiMon());
        //updating row
        return  db.update(TABLE_MONAN,values,COLUMN_MONAN_MA + " = ?",
                new String[]{String.valueOf(mon.getMaMon())});
    }

    public void deleteNote(MonAn mon){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MONAN, COLUMN_MONAN_MA + " = ?",
                new String[]{String.valueOf(mon.getMaMon())});
        db.close();
    }

}
