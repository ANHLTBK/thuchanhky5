package com.ltmt2.kiemtraandroid.model;

import java.io.Serializable;

public class MonAn implements Serializable {
    public int maMon;
    public String tenMon;
    public int giaTien;
    public String loaiMon;

    public MonAn(){}
    public MonAn(String tenMon, int giaTien, String loaiMon) {
        this.tenMon = tenMon;
        this.giaTien = giaTien;
        this.loaiMon = loaiMon;
    }

    public MonAn(int maMon, String tenMon, int giaTien, String loaiMon) {
        this.maMon = maMon;
        this.tenMon = tenMon;
        this.giaTien = giaTien;
        this.loaiMon = loaiMon;
    }

    public int getMaMon() {
        return maMon;
    }

    public void setMaMon(int maMon) {
        this.maMon = maMon;
    }

    public String getTenMon() {
        return tenMon;
    }

    public void setTenMon(String tenMon) {
        this.tenMon = tenMon;
    }

    public int getGiaTien() {
        return giaTien;
    }

    public void setGiaTien(int giaTien) {
        this.giaTien = giaTien;
    }

    public String getLoaiMon() {
        return loaiMon;
    }

    public void setLoaiMon(String loaiMon) {
        this.loaiMon = loaiMon;
    }

    @Override
    public String toString() {
        return this.tenMon;
    }
}
