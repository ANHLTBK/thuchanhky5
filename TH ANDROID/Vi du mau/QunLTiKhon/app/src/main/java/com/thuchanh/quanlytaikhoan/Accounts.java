package com.thuchanh.quanlytaikhoan;

import java.io.Serializable;

public class Accounts implements Serializable {
    private String username;
    private String password;
    public Accounts(String acc_user, String acc_pwd) {
        this.username = acc_user;
        this.password = acc_pwd;
    }
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
}
