package com.thuchanh.quanlytaikhoan;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AccountsActivity extends AppCompatActivity {
    //Khai báo các biến
    EditText txtUsername, txtPassword;
    Button btnSaveExit, btnSave;
    boolean isUpdate;
    Accounts accounts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accounts);

        //Lấy các điều khiển qua ID
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnSaveExit = (Button) findViewById(R.id.btnSaveExit);
        btnSave = (Button) findViewById(R.id.btnSave);

        isUpdate = false;
        Intent i = getIntent();
        Bundle b = i.getBundleExtra("mode");
        if (b != null) {
            isUpdate = b.getBoolean("mode_update");
            if (isUpdate) {
                accounts = (Accounts) b.getSerializable("data");
                txtUsername.setText(accounts.getUsername());
                txtUsername.setEnabled(false);
                txtPassword.setText(accounts.getPassword());
            }
        }
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
            }
        });
        btnSaveExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save();
                finish();
            }
        });
    }
    private void save(){
        String accUser = txtUsername.getText().toString();
        String accPwd = txtPassword.getText().toString();
        if (!isUpdate) {
            txtUsername.setEnabled(true);
            new WSHandling(AccountsActivity.this, accUser, accPwd).execute(
                    WSHandling.CREATE);
            Toast.makeText(this, "Successfull", Toast.LENGTH_SHORT).show();
        } else {
            new WSHandling(AccountsActivity.this, accUser, accPwd).execute(
                    WSHandling.UPDATE);
            Toast.makeText(this, "Successfull", Toast.LENGTH_SHORT).show();
        }
    }
}
