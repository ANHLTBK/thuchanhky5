package com.thuchanh.readxmldata;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.FileInputStream;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class MainActivity extends AppCompatActivity {

    //Khai báo các biến
    Button btnLoadFile;
    Spinner spTitle;
    ListView lvData;
    //Các đối tượng ArrayList và Adapter đế gắn dữ liệu cho
    //Spinner và ListView
    ArrayList<String> titles = new ArrayList<String>();
    ArrayAdapter<String> adapterSpinner;
    ArrayList<String> datas = new ArrayList<String>();
    ArrayAdapter<String> adapterListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Lấy các điều khiển qua ID
        btnLoadFile = (Button) findViewById(R.id.btnLoadFile);
        spTitle = (Spinner) findViewById(R.id.spTitle);
        lvData = (ListView) findViewById(R.id.lvData);

        adapterSpinner = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, titles);
        spTitle.setAdapter(adapterSpinner);

        adapterListView = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, datas);
        lvData.setAdapter(adapterListView);

        //Xử lý sự kiện nhấn nút Tải dữ liệu
        btnLoadFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadXMLfile();
                adapterSpinner.notifyDataSetChanged();
                adapterListView.notifyDataSetChanged();
            }
        });
    }
    //Xây dựng phương thức tải dữ liệu từ file XML
    public void loadXMLfile(){
        try {
            titles.clear();
            datas.clear();
            //1. Tạo đối tượng Document Builder
            DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = fac.newDocumentBuilder();
            //2. Tạo File Input Stream từ tệp tin XML nguồn
            //Lấy đường dẫn đến sdcard
            String sdcard = Environment.getExternalStorageDirectory().getAbsolutePath();
            //Đường dẫn đến file XML
            String xmlfile = sdcard + "/employee.xml";
            FileInputStream fIn = new FileInputStream(xmlfile);
            //3. Dùng phương thức parse của đối tượng Builder để tạo Document
            Document doc=builder.parse(fIn);
            //4. Duyệt từng node trong file XML
            //Lấy node gốc (root)
            Element root= doc.getDocumentElement();
            //Lấy toàn bộ node con của Root
            NodeList list= root.getChildNodes();
            //Duyệt từ node đầu tiên cho tới node cuối cùng
            for(int i=0; i < list.getLength(); i++){
                //Mỗi lần duyệt lấy ra 1 node
                Node node=list.item(i);
                //Kiểm tra xem node đó có phải là Element hay không,
                //Vì ta dựa vào element để lấy dữ liệu bên trong
                if(node instanceof Element){
                    //Lấy được phần tử có tag employee
                    Element elementEmp = (Element) node;

                    //Lấy giá trị thuộc tính ID
                    //id là thuộc tính của tag Employee
                    String id = elementEmp.getAttribute("id");

                    //Lấy giá trị thuộc tính title
                    //title là thuộc tính của tag Employee
                    String title = elementEmp.getAttribute("title");

                    //Lấy tag Name bên trong của tag Employee
                    NodeList listChild = elementEmp.getElementsByTagName("name");

                    //Lấy nội dung của tag name, phần text giữa tag mở và tag đóng
                    //Chỉ cần xử lý item-0 vì chỉ có 1 tag name trong tag Employee
                    String name = listChild.item(0).getTextContent();

                    //Lấy tag phone bên trong của tag Employee
                    listChild = elementEmp.getElementsByTagName("phone");

                    //Lấy nội dung của tag phone
                    //Chỉ cần xử lý item-0 vì chỉ có 1 tag phone trong tag Employee
                    String phone = listChild.item(0).getTextContent();

                    //Gán các dữ liệu trên vào array list
                    titles.add(title);
                    datas.add(id + " - " + name + " - " + phone);
                }
            }
        }catch (Exception e){
            Log.e("ERROR",e.getMessage());
        }
    }
}
