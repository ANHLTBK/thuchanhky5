package com.thuchanh.jsondata;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    //Khai báo các biến
    EditText txtContent;
    Button btnLoadData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Lấy các điều khiển qua ID
        txtContent = (EditText) findViewById(R.id.txtContent);
        btnLoadData = (Button) findViewById(R.id.btnLoadData);
        btnLoadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    //Đọc res/raw/company.json và trả về object Company
                    Company company = ReadJSONData.
                            readCompanyJSONFile(MainActivity.this);
                    txtContent.setText(company.toString());
                } catch(Exception e)  {
                    txtContent.setText(e.getMessage());
                    e.printStackTrace();
                }
            }
        });
    }
}
