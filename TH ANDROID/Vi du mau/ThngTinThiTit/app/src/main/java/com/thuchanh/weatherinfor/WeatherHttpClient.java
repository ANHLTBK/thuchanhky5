package com.thuchanh.weatherinfor;

import android.util.Log;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class WeatherHttpClient {
    //link api cung cấp kết quả thời tiết
    private static String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q=";
    //link image trạng thái thời tiết
    private static String IMG_URL = "http://openweathermap.org/img/wn/";
    //mã APPID nhận được sau khi đăng ký tài khoản trên web openweathermap
    private static String APPID = "&appid=be8d3e323de722ff78208a7dbb2dcd6f";
    //hàm lấy về chuỗi JSON có chứa các thông tin về weather trên web openweathermap,
    //cần nhập vào tên thành phố hoặc địa điểm cần tìm kiếm
    public String getWeatherData(String location) {
        HttpURLConnection con = null ;
        InputStream is = null;
        try {
            con = (HttpURLConnection) ( new
                    URL(BASE_URL + location + APPID)).openConnection();
            con.setRequestMethod("GET");
            con.connect();
            // Đọc các kết quả trả về
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            while ( (line = br.readLine()) != null )
                buffer.append(line + "\r\n");
            is.close();
            con.disconnect();
            return buffer.toString();
        }
        catch(Throwable t) {
            t.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }
        return null;
    }
    //hàm lấy về image
    public byte[] getImage(String code) {
        HttpURLConnection con = null ;
        InputStream is = null;
        try {
            con = (HttpURLConnection) ( new
                    URL(IMG_URL + code + "@2x.png")).openConnection();
            con.setRequestMethod("GET");
            con.connect();
            // Đọc kết quả trả về vã mã hóa chúng
            is = con.getInputStream();
            byte[] buffer = new byte[1024];
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            while ( is.read(buffer) != -1)
                baos.write(buffer);
            return baos.toByteArray();
        }
        catch(Throwable t) { t.printStackTrace(); }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }
        return null;
    }
}
