package com.thuchanh.weatherinfor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONWeatherParser {
    //lấy đối tượng qua tagName
    private static JSONObject getObject(String tagName, JSONObject jObj)
            throws JSONException {
        JSONObject subObj = jObj.getJSONObject(tagName);
        return subObj;
    }
    //lấy chuỗi Json qua tagName
    private static String getString(String tagName, JSONObject jObj)
            throws JSONException {
        if(jObj.has(tagName)) return jObj.getString(tagName);
        else return "";
    }
    //lấy giá trị số float qua tagName
    private static float getFloat(String tagName, JSONObject jObj)
            throws JSONException {
        if(jObj.has(tagName)) return (float) jObj.getDouble(tagName);
        else return 0;
    }
    //lấy giá trị số int qua tagName
    private static int getInt(String tagName, JSONObject jObj)
            throws JSONException {
        if(jObj.has(tagName)) return jObj.getInt(tagName);
        else return 0;
    }
    //lấy về đối tượng Weather được chuyểnn sang từ dữ liệu JSON
    public static Weather getWeather(String data) throws JSONException {
        Weather weather = new Weather();
        // Tạo đối tượng JSONObject từ chuỗi JSON trả về
        JSONObject jObj = new JSONObject(data);

        // Bắt đầu giải mã các thông tin về Location
        Location loc = new Location();

        JSONObject coordObj = getObject("coord", jObj);
        loc.setLatitude(getFloat("lat", coordObj));
        loc.setLongitude(getFloat("lon", coordObj));

        JSONObject sysObj = getObject("sys", jObj);
        loc.setCountry(getString("country", sysObj));
        loc.setSunrise(getInt("sunrise", sysObj));
        loc.setSunset(getInt("sunset", sysObj));
        loc.setCity(getString("name", jObj));
        weather.location = loc;

        // Lấy mảng thông tin thời tiết từ tagName là weather
        JSONArray jArr = jObj.getJSONArray("weather");

        //Chỉ sử dụng phần tử weather đầu tiên (phần tử có chỉ số 0)
        JSONObject JSONWeather = jArr.getJSONObject(0);
        weather.currentCondition.setWeatherId(getInt("id", JSONWeather));
        weather.currentCondition.setDescr(getString("description", JSONWeather));
        weather.currentCondition.setCondition(getString("main", JSONWeather));
        weather.currentCondition.setIcon(getString("icon", JSONWeather));
        JSONObject mainObj = getObject("main", jObj);
        weather.currentCondition.setHumidity(getInt("humidity", mainObj));
        weather.currentCondition.setPressure(getInt("pressure", mainObj));
        weather.temperature.setMaxTemp(getFloat("temp_max", mainObj));
        weather.temperature.setMinTemp(getFloat("temp_min", mainObj));
        weather.temperature.setTemp(getFloat("temp", mainObj));
        // Gió
        JSONObject wObj = getObject("wind", jObj);
        weather.wind.setSpeed(getFloat("speed", wObj));
        weather.wind.setDeg(getFloat("deg", wObj));
        // Mây
        JSONObject cObj = getObject("clouds", jObj);
        weather.clouds.setPerc(getInt("all", cObj));
        return weather;
    }
}
