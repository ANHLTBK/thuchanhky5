package com.thuchanh.weatherinfor;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;

public class MainActivity extends AppCompatActivity {
    //các view để hiển thị kết quả thời tiết
    private TextView cityText;
    private TextView condDescr;
    private TextView temp;
    private TextView press;
    private TextView windSpeed;
    private TextView windDeg;
    private TextView hum;
    private ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        String city = "Hanoi,VN";//địa điểm là Hanoi
        //lấy các điều khiển qua ID
        cityText = (TextView) findViewById(R.id.cityText);
        condDescr = (TextView) findViewById(R.id.condDescr);
        temp = (TextView) findViewById(R.id.temp);
        hum = (TextView) findViewById(R.id.hum);
        press = (TextView) findViewById(R.id.press);
        windSpeed = (TextView) findViewById(R.id.windSpeed);
        windDeg = (TextView) findViewById(R.id.windDeg);
        imgView = (ImageView) findViewById(R.id.condIcon);

        // bắt đầu chạy asynctask xử lý keyword là city truyền vào
        JSONWeatherTask task = new JSONWeatherTask();
        task.execute(new String[]{city});
    }
    //class JSONWeatherTask kế thừa từ AsyncTask thực hiện việc
    //lấy dữ liệu từ API theo địa điểm tìm kiếm qua class
    //WeatherHttpClient' sau đó trả về chuỗi JSON chứa kết quả về Weather
    //và thực hiện việc parse dữ liệu qua class 'JSONParserWeather
    private class JSONWeatherTask extends AsyncTask {
        @Override
        protected Weather doInBackground(Object... params) {
            //thực hiện tiến trình chạy ngầm rồi trả về đối tượng weather
            Weather weather = new Weather();
            String data = ( (new WeatherHttpClient()).getWeatherData(
                    (String)params[0]));
            try {
                weather = JSONWeatherParser.getWeather(data);
                weather.iconData = ( (new WeatherHttpClient()).getImage(
                        weather.currentCondition.getIcon()));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return weather;
        }
        @Override
        protected void onPostExecute(Object w) {
            super.onPostExecute(w);
            Weather weather = (Weather) w;
            //khi lấy và parser dữ liệu xong thì hiển thị lên màn hình
            if (weather.iconData != null && weather.iconData.length > 0) {
                Bitmap img = BitmapFactory.decodeByteArray(weather.iconData,
                        0, weather.iconData.length);
                imgView.setImageBitmap(img);
            }
            cityText.setText(weather.location.getCity() + "," +
                    weather.location.getCountry());
            condDescr.setText(weather.currentCondition.getCondition() +
                    "(" + weather.currentCondition.getDescr() + ")");
            temp.setText("" + Math.round((weather.temperature.getTemp() -
                    273.15)) + "độ C");
            hum.setText("" + weather.currentCondition.getHumidity() + "%");
            press.setText("" + weather.currentCondition.getPressure() + " hPa");
            windSpeed.setText("" + weather.wind.getSpeed() + " mps");
            windDeg.setText("" + weather.wind.getDeg() + "");
        }
    }
}
