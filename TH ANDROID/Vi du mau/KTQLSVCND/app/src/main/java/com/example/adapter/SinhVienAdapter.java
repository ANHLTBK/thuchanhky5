package com.example.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ktqlsvcnd.R;
import com.example.model.SinhVien;

import java.util.ArrayList;

public class SinhVienAdapter extends ArrayAdapter {
    Activity context;
    int layoutId;
    ArrayList<SinhVien> arrSinhVien;
    public SinhVienAdapter(Activity context, int textViewResourceId, ArrayList<SinhVien> objects){
        super(context, textViewResourceId, objects);
        this.context = context;
        this.layoutId = textViewResourceId;
        this.arrSinhVien = objects;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Gán layout vào phần mã nguồn
        convertView = context.getLayoutInflater().inflate(layoutId, null);
        //Lấy các điều khiển theo id
        TextView txttensinhvien = (TextView) convertView.findViewById(R.id.txtTenSinhVien);
        TextView txtdiachi = (TextView) convertView.findViewById(R.id.txtDiaChi);
        ImageView imgview = (ImageView)convertView.findViewById(R.id.imageView);
        //Lấy sinh viên tại vị trí position, đưa vào phần mô tả ngắn
        SinhVien sinhvien = arrSinhVien.get(position);
        txttensinhvien.setText(sinhvien.toString());
        //Lấy thông tin đưa vào phần mô tả chi tiết

        String diachi = "Địa Chỉ : " + sinhvien.getDiaChi();
        //Kiểm tra giới tính để gán cho đúng hình đại diện
        imgview.setImageResource(R.drawable.boyicon);
        if(sinhvien.getGioiTinh().equals("Nu")){
            imgview.setImageResource(R.drawable.girlicon);
        }
        txtdiachi.setText(diachi);
        return convertView;
    }
}
