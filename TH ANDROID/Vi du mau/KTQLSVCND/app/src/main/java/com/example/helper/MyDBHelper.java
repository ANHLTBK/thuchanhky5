package com.example.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.model.SinhVien;

import java.util.ArrayList;

public class MyDBHelper  extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "QLSINHVIEN";
    // Table Name: Note.
        private static final String TABLE_SINHVIEN = "SinhVien";
    private static final String COLUMN_SINHVIEN_MA = "ma_SinhVien";
    private static final String COLUMN_SINHVIEN_TEN= "ten_SinhVien";
    private static final String COLUMN_SINHVIEN_DIACHI = "diachi_SinhVien";
    private static final String COLUMN_SINHVIEN_GIOITINH = "gioitinh_SinhVien";

    public MyDBHelper(Context context){super(context,DATABASE_NAME,null,DATABASE_VERSION);}
    @Override
    public void onCreate(SQLiteDatabase db) {
        String script = "CREATE TABLE " + TABLE_SINHVIEN + "("
                + COLUMN_SINHVIEN_MA + " INTEGER PRIMARY KEY,"
                + COLUMN_SINHVIEN_TEN + " TEXT,"
                + COLUMN_SINHVIEN_DIACHI + " TEXT,"
                + COLUMN_SINHVIEN_GIOITINH + " TEXT" + ")";

        db.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SINHVIEN);
        // Va tao lai
        onCreate(db);
    }
    // Nếu trong bảng Note chưa có dữ liệu,
    // thêm vào mặc đinhj 2 bản ghi
    public void createDefaultNotesIfNeed(){
        int count = this.getSachCount();
        if(count == 0){
            SinhVien sinhVien1 = new SinhVien(1, "anhkool", "hung yen", "Nam");
            SinhVien sinhVien2 = new SinhVien(2, "tuananh", "hung yen", "Nam");

            this.addSinhVien(sinhVien1);
            this.addSinhVien(sinhVien2);
        }
    }
    public int getSachCount(){
        String countQuery = "SELECT * FROM " + TABLE_SINHVIEN;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void addSinhVien(SinhVien sinhVien){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SINHVIEN_TEN, sinhVien.getTenSV());
        values.put(COLUMN_SINHVIEN_DIACHI, sinhVien.getDiaChi());
        values.put(COLUMN_SINHVIEN_GIOITINH, sinhVien.getGioiTinh());

        // Chèn 1 dòng dữ liệu vào bảng
        db.insert(TABLE_SINHVIEN, null,values);
        //Close connect
        db.close();
    }
    public SinhVien getSach(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SINHVIEN, new String[]{COLUMN_SINHVIEN_TEN,
                        COLUMN_SINHVIEN_DIACHI,
                        COLUMN_SINHVIEN_GIOITINH},
                COLUMN_SINHVIEN_MA + "=?",
                new String[]{String.valueOf(id)},
                null, null, null,null);
        if(cursor != null)
            cursor.moveToFirst(); // Di chuyển con trỏ lên đầu bảng
        SinhVien sinhVien = new SinhVien(cursor.getString(0),
                cursor.getString(1), cursor.getString(2));
        //return note
        return sinhVien;
    }
    public ArrayList<SinhVien> getAllSinhVien(){
        ArrayList<SinhVien> noteList = new ArrayList<SinhVien>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_SINHVIEN;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        // Duyệt trên con trỏ, và thêm vào danh sách
        if(cursor.moveToFirst()){
            do{
                SinhVien sinhVien = new SinhVien();
                sinhVien.setMaSV(Integer.parseInt(cursor.getString(0)));
                sinhVien.setTenSV(cursor.getString(1));
                sinhVien.setDiaChi(cursor.getString(2));
                sinhVien.setGioiTinh(cursor.getString(3));

                //Thêm vào danh sách
                noteList.add(sinhVien);
            }while (cursor.moveToNext());
        }
        return noteList;
    }
    public int updateNote(SinhVien sinhVien){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SINHVIEN_TEN, sinhVien.getTenSV());
        values.put(COLUMN_SINHVIEN_DIACHI, sinhVien.getDiaChi());
        values.put(COLUMN_SINHVIEN_GIOITINH, sinhVien.getGioiTinh());
        //updating row
        return  db.update(TABLE_SINHVIEN,values,COLUMN_SINHVIEN_MA + " = ?",
                new String[]{String.valueOf(sinhVien.getMaSV())});
    }
    public void deleteNote(SinhVien sinhVien){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SINHVIEN, COLUMN_SINHVIEN_MA + " = ?",
                new String[]{String.valueOf(sinhVien.getMaSV())});
        db.close();
    }
}
