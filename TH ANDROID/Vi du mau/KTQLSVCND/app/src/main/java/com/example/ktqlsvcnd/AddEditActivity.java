package com.example.ktqlsvcnd;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.helper.MyDBHelper;
import com.example.model.SinhVien;

public class AddEditActivity extends AppCompatActivity {

    SinhVien sinhvien;
    private EditText edtTenSinhVien, edtDiaChi;
    private RadioGroup rggt;
    private RadioButton rNam, rNu;
    private Button btnHuy, btnLuu;
    private boolean needRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit);
        edtTenSinhVien = (EditText) findViewById(R.id.edtTenSinhVien);
        edtDiaChi = (EditText) findViewById(R.id.edtDiaChi);
        rNam = (RadioButton) findViewById(R.id.rNam);
        rNu = (RadioButton) findViewById(R.id.rNu);

        btnHuy = (Button) findViewById(R.id.btnHuy);
        btnLuu = (Button) findViewById(R.id.btnLuu);

        btnHuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSinhVien();
            }
        });
    }
    private void saveSinhVien() {
        MyDBHelper db = new MyDBHelper(this);
        String ten = this.edtTenSinhVien.getText().toString();
        String diachi = this.edtDiaChi.getText().toString();

        if(ten.equals("") || diachi.equals("")){
            Toast.makeText(getApplicationContext(), "Hãy nhập nội dung", Toast.LENGTH_SHORT).show();
            return;
        }
        else {

            String gioitinh = "Nam";
            if(rNu.isChecked()){
                gioitinh = "Nu";
            }
            this.sinhvien = new SinhVien(ten,diachi, gioitinh);
            db.addSinhVien(sinhvien);
        }
        this.needRefresh = true;
        // tro lai MainAc..
        this.onBackPressed();
    }
    @Override
    public void finish() {
        //Chuan bi du lieu Intent
        Intent data = new Intent();
        //Yeu cau Main refresh lai ListView hoac khong
        data.putExtra("needRefresh", needRefresh);
        // Activity da hoan thanh OK, tra ve du lieu
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }
}
