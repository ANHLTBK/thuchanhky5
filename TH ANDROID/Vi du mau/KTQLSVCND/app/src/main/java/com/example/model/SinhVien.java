package com.example.model;

import java.io.Serializable;

public class SinhVien implements Serializable {

    public int MaSV;
    public String TenSV;
    public String DiaChi;
    public String GioiTinh;

    public SinhVien(String tenSV, String diaChi, String gioiTinh) {
        TenSV = tenSV;
        DiaChi = diaChi;
        GioiTinh = gioiTinh;
    }



    public SinhVien() {
    }



    public SinhVien(String tenSV, String diaChi) {
        TenSV = tenSV;
        DiaChi = diaChi;
    }


    public SinhVien(int maSV, String tenSV, String diaChi, String gioiTinh) {
        MaSV = maSV;
        TenSV = tenSV;
        DiaChi = diaChi;
        GioiTinh = gioiTinh;
    }



    public void setMaSV(int maSV) {
        MaSV = maSV;
    }

    public void setTenSV(String tenSV) {
        TenSV = tenSV;
    }

    public void setDiaChi(String diaChi) {
        DiaChi = diaChi;
    }

    public void setGioiTinh(String gioiTinh) {
        GioiTinh = gioiTinh;
    }



    public int getMaSV() {
        return MaSV;
    }

    public String getTenSV() {
        return TenSV;
    }

    public String getDiaChi() {
        return DiaChi;
    }

    public String getGioiTinh() {
        return GioiTinh;
    }


    @Override
    public String toString() {
        return this.TenSV;
    }


}
