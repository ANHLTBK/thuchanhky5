package com.example.ktqlsvcnd;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.adapter.SinhVienAdapter;
import com.example.helper.MyDBHelper;
import com.example.model.SinhVien;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private static final int MENU_ITEM_ADD = 111;
    private static final int MENU_ITEM_EDIT = 444;
    private static final int MENU_ITEM_VIEW = 333;
    private static final int MENU_ITEM_DELETE = 222;
    private static final int MY_REQUEST_CODE = 100;
    ListView lvSinhVien;
    private ArrayList<SinhVien> listSinhvien = new ArrayList<SinhVien>();
    private SinhVienAdapter adaptersinhvien = null;
    private SinhVien SinhvienSelected = null;
    MyDBHelper db = new MyDBHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Lấy về các điều khiển qua ID
        lvSinhVien = (ListView) findViewById(R.id.lvSinhVien);

        //Tạo CSDL, khởi tạo dữ liệu mẫu và gán vào ListView
        MyDBHelper db = new MyDBHelper(this);
        db.createDefaultNotesIfNeed();

        listSinhvien.addAll(db.getAllSinhVien());
        // đẩy dl vào Adapter
        // Định nghĩa một Adapter gồm các tham số
        // 1 - Context
        // 2 - Layout cho các dòng
        // 3 - Danh sách dữ liệu
        adaptersinhvien = new SinhVienAdapter(this,R.layout.listview_custom,listSinhvien);

        // Đăng ký Adapter cho ListView.
        lvSinhVien.setAdapter(adaptersinhvien);

        // Đăng ký Context menu cho ListView.
        registerForContextMenu(lvSinhVien);
    }
    // sử lý xự kiện nhấn dữ chuột
    //Tạo menu ngữ cảnh (context menu)
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Chọn hành động");
        //Các tham số: groupId, itemID, order, title
        menu.add(0, MENU_ITEM_ADD , 0, "Thêm Sinh Viên");
        menu.add(0,MENU_ITEM_DELETE, 1, "Xóa");
        menu.add(0, MENU_ITEM_VIEW , 2, "Xem Sin Viên");
        menu.add(0, MENU_ITEM_EDIT , 3, "Sửa");

    }
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final SinhVien selectSinhVien =(SinhVien) lvSinhVien.getItemAtPosition(info.position);
        if(item.getItemId() == MENU_ITEM_VIEW){
            // Toast là kiểu hiển thị nên màn hình trong thời gian ngắn
            Toast.makeText(getApplicationContext(),
                    selectSinhVien.getTenSV(),Toast.LENGTH_LONG).show();
        }
        else if(item.getItemId() == MENU_ITEM_ADD){
           // Intent chuyền dữ liệu dauwx các activity
            Intent intent = new Intent(this, AddEditActivity.class);
            //Mở giao diện AddEditNoteActivity để thêm mới, có phản hồi
            this.startActivityForResult(intent, MY_REQUEST_CODE);
        }
        else if(item.getItemId() == MENU_ITEM_EDIT ){
            Intent intent = new Intent(this, AddEditActivity.class);
            intent.putExtra("note", selectSinhVien);
            //Mở giao diện AddEditNoteActivity để sửa, có phản hồi
            //Gửi kèm theo ghi chú đang được chọn trong danh sách
            this.startActivityForResult(intent,MY_REQUEST_CODE);
        }
        else if(item.getItemId() == MENU_ITEM_DELETE)
        {
            //Hỏi trước khi xóa
            new AlertDialog.Builder(this)
                    .setTitle("Xác nhận xóa dữ liệu")
                    .setMessage("Bạn có chắc chắn muốn xóa " + selectSinhVien.getTenSV() + " ?")
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteSinhVien(selectSinhVien);
                        }
                    })
                    .setNegativeButton("Không", null)
                    .show();
        }
        else {
            return false;
        }
        return true;
    }
    public void deleteSinhVien(SinhVien sinhVien){
        MyDBHelper db = new MyDBHelper(this);
        db.deleteNote(sinhVien);
        this.listSinhvien.remove(sinhVien);
        // Refresh Listview
        this.adaptersinhvien.notifyDataSetChanged();
    }
    // Khi AddEditNoteActivity hoàn thành, nó gửi phản hồi lại
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == MY_REQUEST_CODE) {
            reloadSinhVien();
        }
    }
    private void reloadSinhVien() {
        listSinhvien.clear();
        ArrayList<SinhVien> list = db.getAllSinhVien();
        listSinhvien.addAll(list);
        this.adaptersinhvien.notifyDataSetChanged();
    }
}
