package com.example.qlsach;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.qlsach.helper.DBHeper;
import com.example.qlsach.model.Sach;

public class AddSach extends AppCompatActivity {
    Sach sach;
    private EditText edtTen, edtGia;
    private RadioGroup rgLoai;
    private RadioButton rLapTrinh, rTKDH;
    private Button btnXoa, btnLuu;
    private boolean needRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_sach);

        edtTen = (EditText) findViewById(R.id.edtTenSach);
        edtGia = (EditText) findViewById(R.id.edtGiaTien);
        rLapTrinh = (RadioButton) findViewById(R.id.rlapTrinh);
        rTKDH = (RadioButton) findViewById(R.id.rTKDH);

        btnXoa = (Button) findViewById(R.id.btnXoa);
        btnLuu = (Button) findViewById(R.id.btnLuu);

        btnXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtTen.setText("");
                edtGia.setText("");
            }
        });

        btnLuu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveSach();
            }
        });
    }
    // Thêm Sách
    private void saveSach() {
        DBHeper db = new DBHeper(this);
        String ten = this.edtTen.getText().toString();
        String gia = this.edtGia.getText().toString();

        if(ten.equals("") || gia.equals("")){
            Toast.makeText(getApplicationContext(), "Hãy nhập nội dung", Toast.LENGTH_SHORT).show();
            return;
        }
        else {
            String loai = "LapTrinh";
            if(rLapTrinh.isChecked()){
                loai = "TKDH";
            }
            this.sach = new Sach(ten, Integer.parseInt(gia), loai);
            db.addSach(sach);
        }
        this.needRefresh = true;
        // tro lai MainAc..
        this.onBackPressed();
    }
    @Override
    public void finish() {
        //Chuan bi du lieu Intent
        Intent data = new Intent();
        //Yeu cau Main refresh lai ListView hoac khong
        data.putExtra("needRefresh", needRefresh);
        // Activity da hoan thanh OK, tra ve du lieu
        this.setResult(Activity.RESULT_OK, data);
        super.finish();
    }
}
