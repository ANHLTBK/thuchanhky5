package com.example.qlsach.model;

import java.io.Serializable;

public class Sach implements Serializable {

    public Sach(String tenSach, int giaTien) {
        this.tenSach = tenSach;
        this.giaTien = giaTien;
    }

    public int maSach;
    public String tenSach;
    public int giaTien;
    public String loaiSach;

    public Sach() {
    }
    public Sach(int maSach, String tenSach, int giaTien, String loaiSach) {
        this.maSach = maSach;
        this.tenSach = tenSach;
        this.giaTien = giaTien;
        this.loaiSach = loaiSach;
    }

    public Sach(String tenSach, int giaTien, String loaiSach) {
        this.tenSach = tenSach;
        this.giaTien = giaTien;
        this.loaiSach = loaiSach;
    }


    public int getMaSach() {
        return maSach;
    }

    public String getTenSach() {
        return tenSach;
    }

    public int getGiaTien() {
        return giaTien;
    }

    public String getLoaiSach() {
        return loaiSach;
    }


    public void setMaSach(int maSach) {
        this.maSach = maSach;
    }

    public void setTenSach(String tenSach) {
        this.tenSach = tenSach;
    }

    public void setGiaTien(int giaTien) {
        this.giaTien = giaTien;
    }

    public void setLoaiSach(String loaiSach) {
        this.loaiSach = loaiSach;
    }

    @Override
    public String toString() {
        return this.tenSach;
    }


}
