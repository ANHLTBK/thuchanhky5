package com.example.qlsach.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.qlsach.R;
import com.example.qlsach.model.Sach;

import java.util.ArrayList;

public class SachAdapter extends ArrayAdapter {
    Activity context;
    int layoutId;

    ArrayList<Sach> arrSach;
    public SachAdapter(Activity context, int textViewResourceId, ArrayList<Sach> objects){
        super(context, textViewResourceId, objects);
        this.context = context;
        this.layoutId = textViewResourceId;
        this.arrSach = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = context.getLayoutInflater().inflate(layoutId, null);

        TextView txtTenSach = (TextView) convertView.findViewById(R.id.txtTenSach);
        TextView txtGia = (TextView) convertView.findViewById(R.id.txtGiaTien);
        ImageView imgview = (ImageView)convertView.findViewById(R.id.imgview);

        Sach sach = arrSach.get(position);
        txtTenSach.setText(sach.toString());

        String stringGia = "Năm Xuất Bản: " + sach.getGiaTien();

        imgview.setImageResource(R.drawable.monb);
        if(sach.getLoaiSach().equals("TKDH")){
            imgview.setImageResource(R.drawable.monanb);
        }

        txtGia.setText(stringGia);
        return convertView;
    }
}
