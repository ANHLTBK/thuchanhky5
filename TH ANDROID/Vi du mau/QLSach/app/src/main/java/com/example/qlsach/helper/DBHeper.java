package com.example.qlsach.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.qlsach.model.Sach;

import java.util.ArrayList;

public class DBHeper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "QLSACH";
    // Table Name: Note.
    private static final String TABLE_SACH = "Sach";
    private static final String COLUMN_SACH_MA = "ma_sach";
    private static final String COLUMN_SACH_TEN = "ten_sach";
    private static final String COLUMN_SACH_GIA = "gia_tien";
    private static final String COLUMN_SACH_LOAI = "loai_sach";

    public DBHeper(Context context){super(context,DATABASE_NAME,null,DATABASE_VERSION);}
    @Override
    public void onCreate(SQLiteDatabase db) {
        String script = "CREATE TABLE " + TABLE_SACH + "("
                + COLUMN_SACH_MA + " INTEGER PRIMARY KEY,"
                + COLUMN_SACH_TEN + " TEXT,"
                + COLUMN_SACH_GIA + " INTEGER,"
                + COLUMN_SACH_LOAI + " TEXT" + ")";

        db.execSQL(script);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SACH);
        // Va tao lai
        onCreate(db);
    }
    // Nếu trong bảng Note chưa có dữ liệu,
    // thêm vào mặc đinhj 2 bản ghi
    public void createDefaultNotesIfNeed(){
        int count = this.getSachCount();
        if(count == 0){
            Sach sach1 = new Sach(1, "PHP", 6000, "laptrinh");
            Sach sach2 = new Sach(2, "CSS", 80000, "TKDH");

            this.addSach(sach1);
            this.addSach(sach2);
        }
    }

    public int getSachCount(){
        String countQuery = "SELECT * FROM " + TABLE_SACH;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int count = cursor.getCount();
        cursor.close();
        return count;
    }

    public void addSach(Sach sach){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SACH_TEN, sach.getTenSach());
        values.put(COLUMN_SACH_GIA, sach.getGiaTien());
        values.put(COLUMN_SACH_LOAI, sach.getLoaiSach());

        // Chèn 1 dòng dữ liệu vào bảng
        db.insert(TABLE_SACH, null,values);
        //Close connect
        db.close();
    }
    public Sach getSach(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_SACH, new String[]{COLUMN_SACH_TEN,
                        COLUMN_SACH_GIA,
                        COLUMN_SACH_LOAI},
                COLUMN_SACH_MA + "=?",
                new String[]{String.valueOf(id)},
                null, null, null,null);
        if(cursor != null)
            cursor.moveToFirst(); // Di chuyển con trỏ lên đầu bảng
        Sach sach = new Sach(cursor.getString(0),
                Integer.parseInt(cursor.getString(1)), cursor.getString(2));
        //return note
        return sach;
    }
    public ArrayList<Sach> getAllSach(){
        ArrayList<Sach> noteList = new ArrayList<Sach>();
        // Select All Query
        String selectQuery = "SELECT * FROM " + TABLE_SACH;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery,null);
        // Duyệt trên con trỏ, và thêm vào danh sách
        if(cursor.moveToFirst()){
            do{
                Sach note = new Sach();
                note.setMaSach(Integer.parseInt(cursor.getString(0)));
                note.setTenSach(cursor.getString(1));
                note.setGiaTien(Integer.parseInt(cursor.getString(2)));
                note.setLoaiSach(cursor.getString(3));

                //Thêm vào danh sách
                noteList.add(note);
            }while (cursor.moveToNext());
        }
        return noteList;
    }
    public int updateNote(Sach sach){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_SACH_TEN, sach.getTenSach());
        values.put(COLUMN_SACH_GIA, sach.getGiaTien());
        values.put(COLUMN_SACH_LOAI, sach.getLoaiSach());
        //updating row
        return  db.update(TABLE_SACH,values,COLUMN_SACH_MA + " = ?",
                new String[]{String.valueOf(sach.getMaSach())});
    }

    public void deleteNote(Sach sach){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_SACH, COLUMN_SACH_MA + " = ?",
                new String[]{String.valueOf(sach.getMaSach())});
        db.close();
    }
}
