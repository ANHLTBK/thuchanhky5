package com.example.qlsach;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.qlsach.adapter.SachAdapter;
import com.example.qlsach.helper.DBHeper;
import com.example.qlsach.model.Sach;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //Khai báo các giá trị hằng là các mục menu
    private static final int MENU_ITEM_ADD = 111;
    private static final int MENU_ITEM_EDIT = 444;
    private static final int MENU_ITEM_VIEW = 333;
    private static final int MENU_ITEM_DELETE = 222;
    private static final int MY_REQUEST_CODE = 100;
    ListView lvSach;
    private ArrayList<Sach> listSach = new ArrayList<Sach>();
    private SachAdapter adaptersach = null;
    private ArrayAdapter<Sach> listViewAdapter;
    private Sach monanSelected = null;
    DBHeper db = new DBHeper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Lấy về các điều khiển qua ID
        lvSach = (ListView) findViewById(R.id.lvSach);
        //Tạo CSDL, khởi tạo dữ liệu mẫu và gán vào ListView
        DBHeper db = new DBHeper(this);
        db.createDefaultNotesIfNeed();

        listSach.addAll(db.getAllSach());
        // đẩy dl vào Adapter
        // Định nghĩa một Adapter gồm các tham số
        // 1 - Context
        // 2 - Layout cho các dòng
        // 3 - Danh sách dữ liệu
        adaptersach = new SachAdapter(this,R.layout.listview_custom,listSach);
        // Đăng ký Adapter cho ListView.
        lvSach.setAdapter(adaptersach);

        // Đăng ký Context menu cho ListView.
        registerForContextMenu(lvSach);
    }
    // sử lý xự kiện nhấn dữ chuột
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Chọn hành động");
        //Các tham số: groupId, itemID, order, title
        menu.add(0, MENU_ITEM_ADD , 0, "Thêm Mới Sách");
        menu.add(0,MENU_ITEM_DELETE, 1, "Xóa");
        menu.add(0, MENU_ITEM_VIEW , 2, "Xem Nội Dung Sach");
        menu.add(0, MENU_ITEM_EDIT , 3, "Sửa");

    }
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        final Sach selectSach =(Sach) lvSach.getItemAtPosition(info.position);
        if(item.getItemId() == MENU_ITEM_VIEW){
            Toast.makeText(getApplicationContext(),
                    selectSach.getTenSach(),Toast.LENGTH_LONG).show();
        }
        else if(item.getItemId() == MENU_ITEM_ADD){
            Intent intent = new Intent(this, AddSach.class);
            //Mở giao diện AddEditNoteActivity để thêm mới, có phản hồi
            this.startActivityForResult(intent, MY_REQUEST_CODE);
        }
        else if(item.getItemId() == MENU_ITEM_EDIT ){
            Intent intent = new Intent(this, AddSach.class);
            intent.putExtra("note", selectSach);
            //Mở giao diện AddEditNoteActivity để sửa, có phản hồi
            //Gửi kèm theo ghi chú đang được chọn trong danh sách
            this.startActivityForResult(intent,MY_REQUEST_CODE);
        }
        else if(item.getItemId() == MENU_ITEM_DELETE)
        {
            //Hỏi trước khi xóa
            new AlertDialog.Builder(this)
                    .setTitle("Xác nhận xóa dữ liệu")
                    .setMessage("Bạn có chắc chắn muốn xóa " + selectSach.getTenSach() + " ?")
                    .setCancelable(false)
                    .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            deleteSach(selectSach);
                        }
                    })
                    .setNegativeButton("Không", null)
                    .show();
        }
        else {
            return false;
        }
        return true;
    }
    public void deleteSach(Sach sach){
        DBHeper db = new DBHeper(this);
        db.deleteNote(sach);
        this.listSach.remove(sach);
        // Refresh Listview
        this.adaptersach.notifyDataSetChanged();
    }
    // Khi AddEditNoteActivity hoàn thành, nó gửi phản hồi lại
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == MY_REQUEST_CODE) {
            reloadBook();
        }
    }
    private void reloadBook() {
        listSach.clear();
        ArrayList<Sach> list = db.getAllSach();
        listSach.addAll(list);
        this.adaptersach.notifyDataSetChanged();
    }


}
