package com.thuchanh.quanlysinhvien;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.thuchanh.adapter.LopHocAdapter;
import com.thuchanh.model.ChucVu;
import com.thuchanh.model.LopHoc;
import com.thuchanh.model.SinhVien;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    //Khai báo các Request + Result code
    //để xử lý kết quả khi chuyển giữa các Activity qua Intent
    public static final int MO_ACTIVITY_THEM_SINH_VIEN = 1;
    public static final int MO_ACTIVITY_SUA_SINH_VIEN = 2;
    public static final int THEM_SINH_VIEN_THANHCONG = 3;
    public static final int SUA_SINH_VIEN_THANHCONG = 4;
    public static final int XEM_DS_SINH_VIEN = 5;
    public static final int CAPNHAT_DS_SINH_VIEN_THANHCONG = 6;
    public static final int MO_ACTIVITY_THIET_LAP_LT_LP = 7;
    public static final int THIET_LAP_LT_LP_THANHCONG = 8;
    public static final int MO_ACTIVITY_CHUYENLOP = 9;
    public static final int CHUYENLOP_THANHCONG = 10;
    //Khai báo các biến điều khiển view
    private Button btnLuulophoc;
    private EditText editMalophoc, editTenlophoc;
    private ListView lvLophoc;
    //Khai báo danh sách các lớp học
    private static ArrayList<LopHoc> arrLopHoc = new ArrayList<LopHoc>();
    //Khai báo LopHocAdapter
    private LopHocAdapter adapterLophoc = null;
    //Biến đánh dấu đối lượng lớp học được chọn trong danh sách
    private LopHoc lophocSelected = null;

    //Phương thức khởi tạo dữ liệu ban đầu cho lớp học
    public void initLopHocData(){
        SinhVien sv = null;
        LopHoc lh = new LopHoc("ltmt4","Lập trình máy tính 4 K9");
        sv = new SinhVien("CD171308","Nguyễn Anh Dũng", true);
        sv.setChucvu(ChucVu.LopTruong);
        lh.themSv(sv);
        sv = new SinhVien("CD171750","Lưu Việt Anh", true);
        lh.themSv(sv);
        sv = new SinhVien("CD171342","Lương Thị Minh Hằng", false);
        lh.themSv(sv);
        arrLopHoc.add(lh);

        lh = new LopHoc("ltmt3","Lập trình máy tính 3 K9");
        sv = new SinhVien("CD172368","Trần Văn Thứ", true);
        sv.setChucvu(ChucVu.LopTruong);
        lh.themSv(sv);
        sv = new SinhVien("CD171353","Nguyễn Hồng Ngân", false);
        lh.themSv(sv);
        arrLopHoc.add(lh);

        lh = new LopHoc("ltmt2","Lập trình máy tính 2 K9");
        arrLopHoc.add(lh);
        lh = new LopHoc("ltmt1","Lập trình máy tính 1 K9");
        arrLopHoc.add(lh);
        adapterLophoc.notifyDataSetChanged();
    }

    public void getWidgets(){
        btnLuulophoc = (Button) findViewById(R.id.btnLuulophoc);
        editMalophoc = (EditText) findViewById(R.id.editMalophoc);
        editTenlophoc =(EditText) findViewById(R.id.editTenlophoc);
        lvLophoc = (ListView) findViewById(R.id.lvLophoc);
        //khởi tạo đối tượng lớp học adapter
        //dùng layout_item_custom.xml
        adapterLophoc = new LopHocAdapter(this,
                R.layout.layout_item_custom,
                arrLopHoc);
        lvLophoc.setAdapter(adapterLophoc);
        //Gọi phương thức đăng ký context menu cho Listview
        registerForContextMenu(lvLophoc);
    }
    // khoi tao Contextmenu
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.contextmenu_lophoc, menu);
    }

    //Xử lý khi người dùng chọn một mục menu, dựa vào ID của item
    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.itemThemSinhVien:
                ThemSinhVien();
                break;
            case R.id.itemDSSV:
                DanhSachSinhVien();
                break;
            case R.id.itemQLLop:
                ThietLapQLLop();
                break;
            case R.id.itemXoaLopHoc:
                XoaLopHoc();
                break;
        }
        return super.onContextItemSelected(item);
    }

    //Phương thức gán sự kiện cho các control: Button, ListView
    public void addEvents(){
        //Bấm nút lưu để lưu lớp học
        btnLuulophoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                LuuLopHoc();
            }
        });
        //xử lý lưu biến tạm khi nhấn long - click
        //phải dùng cái này để biết được trước đó đã chọn item nào
        // sự kiện khi nghười dùng nhấn dữ vào listView
        lvLophoc.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                //Lưu vết lại đối tượng thứ arg2 trong danh sách
                lophocSelected = arrLopHoc.get(arg2);
                return false;
            }
        });
    }

    //Phương thức xử lý lưu lớp học: đưa lớp học mới vào ArrayList
    //nếu cẩn thận hơn thì nên kiểm tra xem mã lớp có bị trùng
    //gọi adapterLopHoc.notifyDataSetChanged(); để cập nhật ListView
    public void LuuLopHoc(){
        String malophoc = editMalophoc.getText().toString();
        String tenlophoc = editTenlophoc.getText().toString();
        LopHoc lophoc = new LopHoc(malophoc, tenlophoc);
        arrLopHoc.add(lophoc);
        adapterLophoc.notifyDataSetChanged();
    }
    //Phương thức xử lý khi chọn menu Thêm sinh viên
    //hiển thị màn hình thêm sinh viên (với dạng Dialog)
    //sử dụng Intent với phương thức startActivityForResult
    //lắng nghe kết quả tại phương thức onActivityResult
    public void ThemSinhVien(){
        Intent i = new Intent(this, ThemSinhVienActivity.class);
        startActivityForResult(i, MO_ACTIVITY_THEM_SINH_VIEN);
    }

    //Phương thức xử lý khi chọn menu Danh sách sinh viên
    //mở DanhSachNhanVienActivity truyền lớp học qua
    //-->toàn bộ danh sách sinh viên của lớp đó được hiển thị
    //sử dụng Intent truyền dữ liệu với Bundle
    public void DanhSachSinhVien() {
        Intent i = new Intent(this, DanhSachSinhVienActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("LOPHOC", lophocSelected);
        i.putExtra("DATA", bundle);
        startActivityForResult(i, XEM_DS_SINH_VIEN);
    }

    //Phương thức xử lý khi chọn menu Quản lý lớp
    //Lớp trưởng và lớp phó
    //ThietLapQLLopActivity sẽ có 2 ListView
    //- Listview 1 hiển thị dang radiobutton để chỉ chọn 1 lớp trưởng
    //- Listview 2 hiển thị dang checkbox cho phép chọn nhiều lớp phó
    public void ThietLapQLLop(){
        Intent i = new Intent(this, ThietLapQLLopActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("LOPHOC", lophocSelected);
        i.putExtra("DATA", bundle);
        startActivityForResult(i, MO_ACTIVITY_THIET_LAP_LT_LP);
    }
    //Phương thức xử lý khi chọn menu Xoá lớp học
    //Hiển thị AlertDialog xác nhận người dùng có xoá hay không
    public void XoaLopHoc() {
        AlertDialog.Builder builder = new AlertDialog.Builder (this);
        builder.setTitle("Xác nhận xoá dữ liệu");
        builder.setMessage("Bạn có chắc chắn muốn xóa [" + lophocSelected.getTen() + "]");
        builder.setIcon(android.R.drawable.ic_input_delete);
        builder.setNegativeButton("Không", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub
                arg0.cancel();
            }
        });
        builder.setPositiveButton("Đồng ý", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                arrLopHoc.remove(lophocSelected);
                adapterLophoc.notifyDataSetChanged();
            }
        });
        builder.show();
    }

    //Phương thức onActivityResult để xử lý kết quả trả về
    //sau khi gọi startActivityForResult kết thúc
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, resultCode, data);
        //màn hình thêm mới sinh viên trả kết quả về
        if(resultCode==THEM_SINH_VIEN_THANHCONG) {
            Bundle bundle= data.getBundleExtra("DATA");
            SinhVien nv= (SinhVien) bundle.getSerializable("SINHVIEN");
            lophocSelected.themSv(nv);
            adapterLophoc.notifyDataSetChanged();
        }
        //Màn hình thiết lập QL Lớp/ cập nhật danh sách trả kết quả về
        else if(resultCode == THIET_LAP_LT_LP_THANHCONG ||
                resultCode == CAPNHAT_DS_SINH_VIEN_THANHCONG)
        {
            Bundle bundle= data.getBundleExtra("DATA");
            LopHoc pb= (LopHoc) bundle.getSerializable("LOPHOC");
            //Xóa danh sách cũ
            lophocSelected.getListSinhVien().clear();
            //Cập nhật lại toàn bộ danh sách mới
            lophocSelected.getListSinhVien().addAll(pb.getListSinhVien());
            adapterLophoc.notifyDataSetChanged();
        }
    }
    //Thêm một phương thức để ở các Activity khác đều
    //có thể truy suất được danh sách phòng ban tổng thể
    public static ArrayList<LopHoc> getListLopHoc() {
        return arrLopHoc;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWidgets();
        addEvents();
        initLopHocData();
    }
}
