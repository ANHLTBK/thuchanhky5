package com.thuchanh.quanlysinhvien;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.thuchanh.model.ChucVu;
import com.thuchanh.model.SinhVien;

public class ThemSinhVienActivity extends AppCompatActivity {

    //Khai báo biến
    private Button btnXoa,btnLuuSV;
    private EditText editMaSV,editTenSV;
    private RadioButton radNam;
    //Phương thức lấy các điều khiển View qua ID
    public void getWidgets() {
        btnXoa = (Button) findViewById(R.id.btnXoa);
        btnLuuSV = (Button) findViewById(R.id.btnLuuSV);
        editMaSV = (EditText) findViewById(R.id.editMaSV);
        editTenSV = (EditText) findViewById(R.id.editTenSV);
        radNam=(RadioButton) findViewById(R.id.radNam);
    }
    //Phương thức xử lý sự kiện các button trên giao diện
    public void addEvents()
    {
        btnXoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                Xoa();
            }
        });
        btnLuuSV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                LuuSinhVien();
            }
        });
    }

    //Phương thức xoá dữ liệu trong các ô
    public void Xoa() {
        editMaSV.setText("");
        editTenSV.setText("");
        editMaSV.requestFocus();
    }
    //Phương thức lưu thông tin sinh viên
    //truyền sinh viên qua MainActivity
    public void LuuSinhVien() {
        SinhVien sv=new SinhVien();
        sv.setMa(editMaSV.getText()+"");
        sv.setTen(editTenSV.getText()+"");
        sv.setChucvu(ChucVu.SinhVien);
        sv.setGioitinh(!radNam.isChecked());
        //Sử dụng Intent trả kết quả về cho MainActivity
        Intent i=getIntent();
        Bundle bundle=new Bundle();
        bundle.putSerializable("SINHVIEN", sv);
        i.putExtra("DATA", bundle);
        setResult(MainActivity.THEM_SINH_VIEN_THANHCONG, i);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_them_sinh_vien);
        getWidgets();
        addEvents();
    }
}
