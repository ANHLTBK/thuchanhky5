package com.thuchanh.quanlysinhvien;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.ImageButton;
import android.widget.ListView;

import com.thuchanh.model.ChucVu;
import com.thuchanh.model.LopHoc;
import com.thuchanh.model.SinhVien;

import java.util.ArrayList;

public class ThietLapQLLopActivity extends AppCompatActivity {

    //Khai báo các biến
    ListView lvLopTruong,lvLopPho;
    ImageButton imgApply;
    //Các ArrayList và Adapter để chứa thông tin lớp trưởng, lớp phó
    ArrayList<SinhVien> arrSVLopTruong = new ArrayList<SinhVien>();
    ArrayAdapter<SinhVien> adapterSVLopTruong;
    ArrayList<SinhVien> arrSVLopPho = new ArrayList<SinhVien>();
    ArrayAdapter<SinhVien> adapterSVLopPho;
    //Biến trạng thái
    int lastChecked = -1;
    LopHoc lophoc = null;

    //Phương thức lấy các điều khiển View qua ID
    public void getWidgets() {
        imgApply = (ImageButton) findViewById(R.id.imgApply);

        lvLopTruong = (ListView) findViewById(R.id.lvLopTruong);
        lvLopTruong.setTextFilterEnabled(true);
        lvLopTruong.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        addListLopTruongEvents();

        lvLopPho = (ListView) findViewById(R.id.lvLopPho);
        addListLopPhoEvents();

        //Lấy được lớp học gửi qua từ MainActivity
        Intent i = getIntent();
        Bundle bundle= i.getBundleExtra("DATA");
        lophoc = (LopHoc) bundle.getSerializable("LOPHOC");
        //Điền sinh viên vào  danh sách lớp trưởng
        arrSVLopTruong.clear();
        for(SinhVien sv : lophoc.getListSinhVien()){
            sv.setChucvu(ChucVu.SinhVien);
            arrSVLopTruong.add(sv);
        }
        //Điền sinh viên vào  danh sách lớp phó
        arrSVLopPho.clear();
        for(SinhVien sv : lophoc.getListSinhVien()){
            sv.setChucvu(ChucVu.SinhVien);
            arrSVLopPho.add(sv);
        }

        //Thiết lập adapter hiển thị các danh sách
        adapterSVLopTruong = new ArrayAdapter<SinhVien>(this,
                android.R.layout.simple_list_item_single_choice,
                arrSVLopTruong);
        adapterSVLopPho = new ArrayAdapter<SinhVien>(this,
                android.R.layout.simple_list_item_multiple_choice,
                arrSVLopPho);
        lvLopTruong.setAdapter(adapterSVLopTruong);
        lvLopPho.setAdapter(adapterSVLopPho);

        adapterSVLopTruong.notifyDataSetChanged();
        adapterSVLopPho.notifyDataSetChanged();

        //Xử lý sự kiện nhấn Button Apply
        addImgApplyEvents();
    }

    //Phương thức xử lý sự kiện chọn một mục trong danh sách lớp trưởng
    public void addListLopTruongEvents(){
        lvLopTruong.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int i, long l){
                CheckedTextView cv = (CheckedTextView) view;
                if(!cv.isChecked()) {
                    arrSVLopTruong.get(i).setChucvu(ChucVu.SinhVien);
                }
                else{
                    cv.setChecked(true);
                    arrSVLopTruong.get(i).setChucvu(ChucVu.LopTruong);
                }
                lastChecked = i;
            }
        });
    }
    //Phương thức xử lý sự kiện chọn nhiều mục trong danh sách lớp phó
    public void addListLopPhoEvents(){
        lvLopPho.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view,
                                    int i, long l){
                CheckedTextView cv = (CheckedTextView) view;
                if(!cv.isChecked()){
                    cv.setChecked(true);
                    arrSVLopPho.get(i).setChucvu(ChucVu.LopPho);
                }
                else {
                    cv.setChecked(false);
                    arrSVLopPho.get(i).setChucvu(ChucVu.SinhVien);
                }
            }
        });
    }
    //Phương thức gán sự kiện click vào nút Apply
    //Gửi thông tin lại màn hình MainActivity
    public void addImgApplyEvents(){
        imgApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = getIntent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("LOPHOC", lophoc);
                i.putExtra("DATA", bundle);
                setResult(MainActivity.THIET_LAP_LT_LP_THANHCONG, i);
                finish();
            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thiet_lap_qllop);
        getWidgets();
    }
}
