package com.thuchanh.adapter;

import android.widget.ArrayAdapter;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import com.thuchanh.model.LopHoc;
import com .thuchanh.model.SinhVien;
import com.thuchanh.quanlysinhvien.R;

public class LopHocAdapter extends ArrayAdapter<LopHoc> {
    Activity context;
    int layoutId;
    ArrayList<LopHoc> arrLopHoc;
    public LopHocAdapter(Activity context, int textViewResourceId,
                           ArrayList<LopHoc> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.layoutId = textViewResourceId;
        this.arrLopHoc = objects;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Gán layout vào phần mã nguồn
        convertView = context.getLayoutInflater().inflate(layoutId, null);
        //Lấy các điều khiển theo id
        TextView txtlophoc = (TextView) convertView.findViewById(R.id.txtShortInfor);
        TextView txtmotalophoc = (TextView) convertView.findViewById(R.id.txtDetailInfor);
        ImageView imgview = (ImageView) convertView.findViewById(R.id.imgview);
        //Lấy lớp học tại vị trí position, đưa vào phần mô tả ngắn
        LopHoc lophoc = arrLopHoc.get(position);
        txtlophoc.setText(lophoc.toString());
        //Lấy thông tin lớp trưởng, lớp phó đưa vào phần mô tả chi tiết
        String strMota = "";
        String loptruong = "Lớp Trưởng: [Chưa có]";
        SinhVien sv = lophoc.getLopTruong();
        if(sv!=null){
            loptruong = "Lớp Trưởng: [" + sv.getTen() + "]";
        }
        ArrayList<SinhVien> dsLopPho = lophoc.getLopPho();
        String loppho = "Lớp Phó: [Chưa có]";
        if(dsLopPho.size()>0){
            loppho = "Lớp Phó:\n";
            for(int i=0; i<dsLopPho.size(); i++){
                loppho += (i+1) + ". " + dsLopPho.get(i).getTen() + "\n";
            }
        }
        strMota = loptruong + "\n" + loppho;
        //gán thông tin cho phần chi tiết
        txtmotalophoc.setText(strMota);
        //gán ảnh đại diện cho lớp học
        imgview.setImageResource(R.drawable.lophoc);
        return convertView;
    }
}
