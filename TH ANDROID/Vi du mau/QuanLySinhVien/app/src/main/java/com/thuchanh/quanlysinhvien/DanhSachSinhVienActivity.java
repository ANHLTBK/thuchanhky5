package com.thuchanh.quanlysinhvien;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.thuchanh.adapter.SinhVienAdapter;
import com.thuchanh.model.LopHoc;
import com.thuchanh.model.SinhVien;

import java.util.ArrayList;

public class DanhSachSinhVienActivity extends AppCompatActivity {

    //Khai báo các biến
    TextView txtMsg;
    ImageButton btnBack;
    ListView lvSinhvien;
    ArrayList<SinhVien> arrSinhvien = null;
    //Sinh viên Adapter để hiển thị thông tin
    //chi tiết : chức vụ, giới tính
    SinhVienAdapter adapter = null;
    LopHoc lophoc = null;
    //Biến đánh dấu sinh viên được chọn trong danh sách
    private SinhVien sinhvienSelected = null;
    private int position = -1;

    //Phương thức lấy các điều khiển View qua ID
    public void getWidgets() {
        txtMsg=(TextView) findViewById(R.id.txtMsg);
        btnBack=(ImageButton) findViewById(R.id.btnBack);
        lvSinhvien=(ListView) findViewById(R.id.lvSinhvien);
    }
    //Phương thức tải danh sách sinh viên
    //dựa trên lớp học được chọn từ màn hình Main
    //Dữ liệu được lấy về qua Bundle trong Intent
    public void getDataFromMain(){
        Intent i = getIntent();
        Bundle b = i.getBundleExtra("DATA");
        lophoc = (LopHoc) b.getSerializable("LOPHOC");
        arrSinhvien = lophoc.getListSinhVien();
        adapter = new SinhVienAdapter(this,
                R.layout.layout_item_custom,
                arrSinhvien);
        lvSinhvien.setAdapter(adapter);
        txtMsg.setText("Danh sách sinh viên [" + lophoc.getTen() + "]");
    }
    //Phương thức gán sự kiện cho các control: Button, ListView
    public void addEvents(){
        //Sự kiện khi nhấn nút Back
        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                UpdateToMain();
            }
        });
        //Sự kiện bắt mục được chọn trong danh sách khi nhấn giữ lâu
        lvSinhvien.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener(){
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int arg2, long arg3) {
                sinhvienSelected = arrSinhvien.get(arg2);
                position = arg2;
                return false;
            }

        });
    }
    //Phương thức xử lý khi nhấn nút Back
    //Trả về kết quả cho Activity Main
    public void UpdateToMain() {
        Intent i = getIntent();
        Bundle b = new Bundle();
        b.putSerializable("LOPHOC", lophoc);
        i.putExtra("DATA", b);
        setResult(MainActivity.CAPNHAT_DS_SINH_VIEN_THANHCONG, i);
        finish();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_danh_sach_sinh_vien);
        getWidgets();
        getDataFromMain();
        addEvents();
    }
}
