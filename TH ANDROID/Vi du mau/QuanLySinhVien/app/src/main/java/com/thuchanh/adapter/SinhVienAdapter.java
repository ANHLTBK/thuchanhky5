package com.thuchanh.adapter;

import android.widget.ArrayAdapter;
import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.thuchanh.quanlysinhvien.R;
import com.thuchanh.model.SinhVien;
import com.thuchanh.model.LopHoc;
import java.util.ArrayList;

public class SinhVienAdapter extends ArrayAdapter<SinhVien> {
    Activity context;
    int layoutId;
    ArrayList<SinhVien> arrSinhVien;
    public SinhVienAdapter(Activity context, int textViewResourceId,
                           ArrayList<SinhVien> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.layoutId = textViewResourceId;
        this.arrSinhVien = objects;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Gán layout vào phần mã nguồn
        convertView = context.getLayoutInflater().inflate(layoutId, null);
        //Lấy các điều khiển theo id
        TextView txtsinhvien = (TextView) convertView.findViewById(R.id.txtShortInfor);
        TextView txtmotasinhvien = (TextView) convertView.findViewById(R.id.txtDetailInfor);
        ImageView img = (ImageView) convertView.findViewById(R.id.imgview);
        //Lấy sinh viên tại vị trí position, đưa vào phần mô tả ngắn
        SinhVien sv = arrSinhVien.get(position);
        txtsinhvien.setText(sv.toString());
        //Lấy thông tin đưa vào phần mô tả chi tiết
        String strMota = "";
        String cv = "Chức vụ: " + sv.getChucvu().getChucVu();
        String gt = "Giới tính: " + (sv.isGioitinh()?"Nữ":"Nam");
        //Kiểm tra giới tính để gán cho đúng hình đại diện
        img.setImageResource(R.drawable.girlicon);
        if(!sv.isGioitinh())
            img.setImageResource(R.drawable.boyicon);
        strMota = cv + "\n" + gt;
        txtmotasinhvien.setText(strMota);
        return convertView;
    }
}
