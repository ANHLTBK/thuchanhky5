package com.thuchanh.model;

import java.io.Serializable;
import java.util.ArrayList;

public class LopHoc extends ThongTin
        implements Serializable {
    private static final long serialVersionUID = 1L;
    //Danh sách sinh viên của lớp học
    private ArrayList<SinhVien> dssv = new ArrayList<SinhVien>();
    public LopHoc(String ma, String ten) {
        super(ma,ten);
    }
    public LopHoc() {
        super();
    }
    //Thêm một sinh viên mới,
    //nếu sinh viên đã có trong danh sách thì cập nhật thông tin
    public void themSv(SinhVien sv){
        int i=0;
        for(i=0 ; i<dssv.size() ; i++){
            SinhVien nvOld = dssv.get(i);
            if(nvOld.getMa().trim().equalsIgnoreCase(sv.getMa().trim())){
                break;
            }
        }
        if(i<dssv.size())
            dssv.set(i, sv);
        else
            dssv.add(sv);
    }
    //Trả về sinh viên tại vị trí index
    public SinhVien get(int index){
        return dssv.get(index);
    }
    //Số lượng sinh viên trong lớp
    public int size(){
        return dssv.size();
    }
    //Trả về sinh viên là lớp trưởng của lớp
    //Một lớp học chỉ có 1 lớp trưởng
    public SinhVien getLopTruong() {
        for(int i=0; i<dssv.size(); i++) {
            SinhVien sv=dssv.get(i);
            if(sv.getChucvu()==ChucVu.LopTruong)
                return sv;
        }
        return null;
    }
    //Trả về danh sách sinh viên là lớp phó
    //Một lớp học có thể có nhiều lớp phó
    public ArrayList<SinhVien>getLopPho(){
        ArrayList<SinhVien> dsLopPho=new ArrayList<SinhVien>();
        for(SinhVien sv: dssv)
        {
            if(sv.getChucvu()==ChucVu.LopPho)
                dsLopPho.add(sv);
        }
        return dsLopPho;
    }
    //Trả về danh sách toàn bộ sinh viên trong lớp
    public ArrayList<SinhVien> getListSinhVien() {
        return this.dssv;
    }
    @Override
    public String toString() {
        // TODO Auto-generated method stub
        String str=super.toString();
        if(dssv.size()==0)
            str += "\n(Chưa có sinh viên)";
        else
            str += "\n(có "+dssv.size() + " Sinh Viên)";
        return str;
    }
}
