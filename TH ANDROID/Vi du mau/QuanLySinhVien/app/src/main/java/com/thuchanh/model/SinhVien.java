package com.thuchanh.model;

import java.io.Serializable;

public class SinhVien extends ThongTin
        implements Serializable {
    private static final long serialVersionUID = 1L;
    private boolean gioitinh;
    private ChucVu chucvu = ChucVu.SinhVien;
    private LopHoc lophoc;
    public SinhVien(String ma, String ten,
                    boolean gioitinh,ChucVu chucvu,
                    LopHoc lophoc) {
        super(ma,ten);
        this.gioitinh = gioitinh;
        this.chucvu = chucvu;
        this.lophoc = lophoc;
    }
    public SinhVien(String ma, String ten,
                    boolean gioitinh) {
        super(ma,ten);
        this.gioitinh = gioitinh;
    }
    public SinhVien() {
        super();
    }
    public boolean isGioitinh() {
        return gioitinh;
    }
    public void setGioitinh(boolean gioitinh) {
        this.gioitinh = gioitinh;
    }
    public ChucVu getChucvu() {
        return chucvu;
    }
    public void setChucvu(ChucVu chucvu) {
        this.chucvu = chucvu;
    }
    public LopHoc getLophoc() {
        return lophoc;
    }
    public void setLophoc(LopHoc lophoc) {
        this.lophoc = lophoc;
    }
    @Override
    public String toString() {
        return super.toString();
    }
}
