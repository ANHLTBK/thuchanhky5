package com.thuchanh.model;

public enum ChucVu {
    LopTruong("Lớp Trưởng"),
    LopPho("Lớp Phó"),
    SinhVien("Sinh Viên");
    private String chucvu;
    ChucVu(String chucvu){
        this.chucvu=chucvu;
    }
    public String getChucVu(){
        return this.chucvu;
    }
}
