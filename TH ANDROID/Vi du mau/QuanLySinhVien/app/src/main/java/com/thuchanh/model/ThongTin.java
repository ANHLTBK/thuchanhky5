package com.thuchanh.model;

import java.io.Serializable;

public class ThongTin implements Serializable {
    private static final long serialVersionUID = 1L;
    private String ma;
    private String ten;
    public ThongTin(String ma, String ten) {
        super();
        this.ma = ma;
        this.ten = ten;
    }
    public ThongTin() {
        super();
    }
    public String getMa() {
        return ma;
    }
    public void setMa(String ma) {
        this.ma = ma;
    }
    public String getTen() {
        return ten;
    }
    public void setTen(String ten) {
        this.ten = ten;
    }
    @Override
    public String toString() {

        return this.ma + " - " + this.ten;
    }
}
