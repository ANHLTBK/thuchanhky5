-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2019 at 11:50 AM
-- Server version: 10.0.17-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quanly`
--

-- --------------------------------------------------------

--
-- Table structure for table `dangky`
--

CREATE TABLE `dangky` (
  `ID` int(10) NOT NULL,
  `HOTEN` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NOIO` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `EMAIL` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `GIOITINH` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NAME` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `PASSWORD` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NLMK` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `NGAYSINH` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Avatar` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `dangky`
--

INSERT INTO `dangky` (`ID`, `HOTEN`, `NOIO`, `EMAIL`, `GIOITINH`, `NAME`, `PASSWORD`, `NLMK`, `NGAYSINH`, `Avatar`) VALUES
(11, 'abcd', 'HÃ  Ná»™i', 'dung.ckay9999@gmail.com', 'nam', 'abcd', '1', '1', '//', 'Hydrangeas.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dangky`
--
ALTER TABLE `dangky`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dangky`
--
ALTER TABLE `dangky`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
