<!DOCTYPE html>
<html>
<head>
	<title>Đăng ký</title>
</head>
<body>
	<form method="POST" enctype="multipart/form-data">
		<h1 align="center">Đăng ký</h1>
		<table align="center" >
			<tr>
				<td colspan="2">
					<font style="font-weight: bold;">Thông tin đăng nhập</font>
				</td>
			</tr>
			<tr>
				<td>Chọn tên đăng nhập</td>
				<td>
					<input type="text" name="txtUserName">
				</td>
			</tr>
			<tr>
				<td>Mật khẩu</td>
				<td>
					<input type="password" name="txtPassword">
				</td>
			</tr>
			<tr>
				<td>Nhập lại mật khẩu</td>
				<td>
					<input type="password" name="txtRetypePassword">
				</td>
			</tr>
			<tr>
				<td>Ảnh avatar (nếu có)</td>
				<td>
					<input type="file" name="imgAvatar">
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<font style="font-weight: bold;">Thông tin cá nhân</font>
				</td>
			</tr>
			<tr>
				<td>Họ và tên</td>
				<td>
					<input type="text" name="txtName">
				</td>
			</tr>
			<tr>
				<td>Địa chỉ email</td>
				<td>
					<input type="email" name="txtEmail">
				</td>
			</tr>
			<tr>
				<td>Giới tính</td>
				<td>
					<input type="radio" name="radioGender" value="Nam">Nam
					<input type="radio" name="radioGender" value="Nữ">Nữ
				</td>
			</tr>
			<tr>
				<td>Ngày sinh</td>
				<td>
					<input type="date" name="dateBirthday">
				</td>
			</tr>
			<tr>
				<td>Nơi ở</td>
				<td>
					<select name="selectLocation">
						<option value="Hà Nội">Hà Nội</option>
						<option value="Sơn La">Nhà quê</option>
						<option value="Phú Thọ">Phú Thọ</option>
						<option value="Điện Biên">Điện Biên</option>
						<option value="Buôn Mê Thuật">Buôn Mê Thuật</option>
						<option value="Thanh Hóa">Nước bạn</option>
					</select>
				</td>
			</tr>
			<tr>
				<td colspan="2" align="center">
					<input type="submit" name="OK" value="Đăng ký">
				</td>
			</tr>
		</table>
	</form>
	<?php
	if(isset($_POST["OK"])){
		include("connect.php");
		$userName = $_POST["txtUserName"]; 
		$password = $_POST["txtPassword"]; 
		$retypePassword = $_POST["txtRetypePassword"]; 
		$name = $_POST["txtName"]; 
		$email = $_POST["txtEmail"]; 
		$rawGender = $_POST["radioGender"]; 
		$birthday = $_POST["dateBirthday"]; 
		$address = $_POST["selectLocation"];

		$gender = true; 
		if($rawGender=="Nữ"){
			$gender=false; 
		} 

		$regex = "[\W]"; 
		$regexUserName = preg_match($regex, $userName); 
		$regexPassword = preg_match($regex, $password); 
		$sql_querry_user_name = "select * from QuanLy.User where Username = '$userName'"; 
		$query_user_name = mysqli_query($conn, $sql_querry_user_name); 

		if($_FILES["imgAvatar"]["type"]!="image/jpeg"){
			echo "<script>alert('Không đúng định dạng ảnh, chỉ chấp nhận file .jpg')</script>";
		}
		// elseif ($_FILES["imgAvatar"]["size"]>6.25) {
		// 	echo "<script>alert('Ảnh quá lớn, xin mời chọn ảnh có kích thước 512 x 512 trở xuống')</script>";
		// }
		elseif (mysqli_num_rows($query_user_name)>0){
			echo "Tài khoản đã có người đăng ký";
		}
		elseif ($password!=$retypePassword){
			echo "Mật khẩu không khớp, mời thử lại";
		}
		elseif($regexUserName==true || $regexPassword==true){
			echo "Tên tài khoản và mật khẩu không được chứa ký tự đặc biệt hoặc khoảng trắng";
		}
		elseif ($userName==""||$password=="") {
			echo "Tên tài khoản và mật khẩu không được để trống";
		}
		else{
			$a = $_FILES["imgAvatar"]["tmp_name"]; 
			$b = $_FILES["imgAvatar"]["name"]; 
			$c = move_uploaded_file($a,'images/'.$b); 
			$query = "insert into QuanLy.user(username, password, name, address, email, gender, birthday, avatar)
				values('$userName', '$password', '$name', '$address', '$email', '$gender', '$birthday', '$b')"; 
			$insert = mysqli_query($conn, $query);
			if($insert==true){
				header("location: login.php"); 
			}
		}
	}
	?>
</body>
</html>