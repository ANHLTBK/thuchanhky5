<!DOCTYPE html>
<html>
<head>
	<title>
		Sign Up
	</title>
</head>
<body >
	<div style="width: 100%; height: 100%; margin-top: 100px" align="center">
		<div style="margin: auto;">
			<form method="POST">
				<table>
					<tr>
						<td>Họ và tên</td>
						<td>
							<input type="text" name="txtName">
						</td>
					</tr>
					<tr>
						<td>Địa chỉ</td>
						<td>
							<input type="text" name="txtAddress">
						</td>
					</tr>
					<tr>
						<td>Điện thoại</td>
						<td>
							<input type="text" name="txtPhone">
						</td>
					</tr>
					<tr>
						<td>Giới tính</td>
						<td>
							<input type="radio" name="radioGender" value="Nam">Nam
							<input type="radio" name="radioGender" value="Nữ">Nữ
						</td>
					</tr>
					<tr>
						<td>Tên tài khoản</td>
						<td>
							<input type="text" name="txtUserName">
						</td>
					</tr>
					<tr>
						<td>Mật khẩu</td>
						<td>
							<input type="password" name="txtPassword">
						</td>
					</tr>
					<tr>
						<td>Nhập lại mật khẩu</td>
						<td>
							<input type="password" name="txtRetypePassword">
						</td>
					</tr>
					<tr>
						<td>Ghi chú thêm</td>
						<td>
							<textarea cols="20" rows="4" name="txtNote"></textarea>
						</td>
					</tr>
					<tr>
						<td>Ảnh avatar</td>
						<td>
							<input type="file" name="imageAvatar"> 
						</td>
					</tr>
					<tr>
						<td>Sở thích</td>
						<td>
							<input type="checkbox" name="checkboxHobby[]" value="Thể thao">Thể thao
							<input type="checkbox" name="checkboxHobby[]" value="Âm nhạc">Âm nhạc
							<input type="checkbox" name="checkboxHobby[]" value="Game">Game
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input type="submit" name="btnSubmit" value="Đồng ý">
						</td>
					</tr>
				</table>
			</form>
			<?php
				if (isset($_POST["btnSubmit"])) {
					include('connect.php'); 
					$name = $_POST["txtName"]; 
					$address = $_POST["txtAddress"]; 
					$phone = $_POST["txtPhone"]; 
					$gender = $_POST["radioGender"]; 
					$username = $_POST["txtUserName"]; 
					$password = $_POST["txtPassword"]; 
					$retypePassword = $_POST["txtRetypePassword"]; 
					$note = $_POST["txtNote"]; 
					$hobbies = ""; 
					foreach ($_POST['checkboxHobby'] as $selected) {
						$hobbies .= $selected.", "; 
					}
					$genderBool = true; 
					if($gender=="Nữ"){
						$genderBool=false; 
					}
					$regex = "[\W]"; 
					$regexUserName = preg_match($regex, $username); 
					$regexPassword = preg_match($regex, $password); 
					$sql_querry_user_name = "select * from QuanLy.User where Username = '$username'"; 
					$query_user_name = mysqli_query($conn, $sql_querry_user_name); 
					if (mysqli_num_rows($query_user_name)>0){
						echo "Tài khoản đã có người đăng ký";
					}
					elseif ($password!=$retypePassword){
						echo "Mật khẩu không khớp, mời thử lại";
					}
					elseif($regexUserName==true || $regexPassword==true){
						echo "Tên tài khoản và mật khẩu không được chứa ký tự đặc biệt hoặc khoảng trắng";
					}
					elseif ($username==""||$password=="") {
						echo "Tên tài khoản và mật khẩu không được để trống";
					}
					elseif ($_FILES["imageAvatar"]["type"] && $_FILES["imageAvatar"]["type"]!="image/jpeg") {
						echo "<script>alert('Ảnh không đúng định dạng jpeg')</script>";
					}
					else{
						$tempNameImage = $_FILES['imageAvatar']['tmp_name'];
						$imageName = $_FILES['imageAvatar']['name']; 
						$moveToFolder = move_uploaded_file($tempNameImage, 'images/'.$imageName); 
						$sql_insert="insert into QuanLy.User (username, password, name, address, phone, gender, avatar, hobby, note) values ('$username', '$password', '$name', '$address', '$phone', '$genderBool', '$imageName', '$hobbies',"; 
						if($note==""){
							$sql_insert.=" default)"; 
						}else{
							$sql_insert.=" '$note')"; 
						}
						$insert = mysqli_query($conn, $sql_insert); 
						if ($insert==true){
							echo "<script>alert('Thêm dữ liệu thành công')</script>";
						}
					}
				}
			?>
		</div>
	</div>
</body>
</html>