-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 03, 2019 lúc 08:43 AM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.1.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `quanly`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `username` varchar(20) COLLATE utf8mb4_unicode_nopad_ci NOT NULL,
  `PASSWORD` varchar(30) COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL,
  `address` varchar(50) COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL,
  `phone` varchar(12) COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL,
  `gender` tinyint(1) DEFAULT NULL,
  `hobby` varchar(100) COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_nopad_ci DEFAULT 'Không có',
  `birthday` date DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_nopad_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_nopad_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`username`, `PASSWORD`, `name`, `address`, `phone`, `gender`, `hobby`, `note`, `birthday`, `avatar`, `email`) VALUES
('devapp', 'devapp', 'Mobile App Dev', '84 Vũ Trọng Phụng', '0982960442', 1, 'Đọc sách, nghe nhạc, thể thao, chơi game', 'Không có', NULL, NULL, NULL),
('hieuvh301195', 'hieubeobb1995', '', '9D1 ngõ 190 Lò Đúc, Hai Bà Trưng, Hà Nội', '0982960442', 1, 'Thể thao, ', 'Không có', NULL, NULL, NULL),
('tu_1', '1', '', '', '', 1, 'Thể thao, ', 'Không có', NULL, NULL, NULL),
('tu_wjbu', 'wibucucmanh', '', '9D1 ngÃµ 190 LÃ² ÄÃºc, Hai BÃ  TrÆ°ng, HÃ  Ná»™i', '0879854564', 1, 'ThÃªÌ‰ thao, Ã‚m nhaÌ£c, Game, ', 'aspjapsdjp', NULL, NULL, NULL),
('tu21056', 'wjbucucmanh', 'Anh Tú', 'Hà Nội', '097849546', 1, 'Xem anime', 'Không có', NULL, NULL, NULL),
('vanvan', 'vanngu', '', 'Hà Nam', '098785646', 1, 'Thể thao, Âm nhạc, ', '', NULL, NULL, NULL),
('wibu', 'qwe', 'Nguyễn Anh Tú', 'Hà Nội', NULL, 1, NULL, 'Không có', '1999-09-26', 'icon_logo.jpg', 'wibu@gmail.com'),
('wibu2', '123', 'Nguyễn Anh Tú', 'Hà Nội', NULL, 1, NULL, 'Không có', '2019-09-03', '1.jpg', 'wibu@gmail.com'),
('wibu3', '123', 'Trần Thị Thúy Vân', 'Buôn Mê Thuật', NULL, 1, NULL, 'Không có', '2019-09-12', 'BangDiem_VuHuyHieu.jpg', 'wibu@gmail.com');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`username`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
