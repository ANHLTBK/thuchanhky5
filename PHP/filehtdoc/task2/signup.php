<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="../boostrap/css/bootstrap.min.css">
	<script type="text/javascript" src = "../boostrap/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src ="../boostrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container" style="margin-top:50px">
		<div class="row">
			<div class="col-md-4">
			</div>
			<div class="col-md-4" style="background-color: pink">
				<div>
					<form action="#" method="POST" enctype="multipart/form-data">
							<h3>Thông tin đăng nhập</h3>
							<div class="form-group">
								<label class="red" for="#">Chọn tên đăng nhập</label>
								<input type="text" name="txtusername" class="form-control">
							</div>
							<div class="form-group">
								<label class="red" for="#">Mật khẩu</label>
								<input type="password" name="txtpasswd" class="form-control">
							</div>
							<div class="form-group">
								<label class="red" for="#">Nhập lại mật khẩu </label>
								<input type="password" name="txtconfirm" class="form-control">
							</div>
							<div class="form-group">
								<label class="red" for="#">Ảnh avatar(nếu có)</label>
								<input type="file" name="anh">
							</div>
						
						
							<h3>Thông tin cá nhân</h3>
							<div class="form-group">
								<label for="#">Họ và tên</label>
								<input type="text" name="txtfullname" class="form-control">
							</div>
							<div class="form-group">
								<label for="#">Địa chỉ mail</label>
								<input type="text" name="txtmailaddress" class="form-control">
							</div>
							<div class="form-group">
								<label for="#" style="margin-right: 20px">Giới tính</label>
								<input type="radio" name="txtgender" value="Nam"style="margin-right:10px">Nam
								<input type="radio" name="txtgender" value="Nu"style="margin:0 10px">Nữ
							</div>
							<div class="form-group">
								<label for="#">Ngày sinh</label>
								<input type="date" name="txtbirthday" class="form-control">
							</div>
							<div class="form-group">
								<label for="#">Nơi ở</label>
								<select name="txtaddress"class="form-control">
									<option value="Hà Nam">Hà Nam</option>
									<option value="Hà Nội">Hà Nội</option>
									<option value="Sài Gòn">Sài Gòn</option>
								</select>
							</div>
						<div style="padding:10px 30px;display: flex;justify-content: space-around;">
							<button class="btn btn-primary" type="submit" name="OK">Sign-up</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php 
		if(isset($_POST['OK'])){
			$username = $_POST['txtusername'];
			$passwd = $_POST['txtpasswd'];
			$confirmpw = $_POST['txtconfirm'];
			$fullname = $_POST['txtfullname'];
			$mailAddress = $_POST['txtmailaddress'];
			$gender = $_POST['txtgender'];
			$birthday = $_POST['txtbirthday'];
			$address = $_POST['txtaddress'];
			if($username == null || $passwd == null){
				echo "<script> alert('username or password không được để trống')</script>";
				die();
			};
			if($passwd != $confirmpw){
				echo "<script> alert('password và nhập lại mật khẩu phải trùng nhau')</script>";
				die();
			};
			if(strlen($passwd)<6){
				echo "<script> alert('password phải có ít nhất 6 ký tự')</script>";
				die();
			}


			include 'connect.php';
			$query = "select * from appuser where Username = '$username'";
			$execute = mysqli_query($con,$query);
			$result = mysqli_num_rows($execute);
			if($result == 1){
				echo"<script> alert('username đã tồn tại')</script>";
				die();
			}

			
			if($_FILES['anh']['type'] !='image/jpeg'){
				echo"<script> alert('không đúng định dạng fdfaffdf')</script>";
				die();
			}
			$fileTmpPath = $_FILES['anh']['tmp_name'];
			echo $fileTmpPath;
			$pathName = $_FILES['anh']['name'];
			echo $pathName;
			$upLoaded = move_uploaded_file($fileTmpPath, 'img/'.$pathName);
			$queryInsert = "insert into appuser(Username,passwd,avatar,HoTen,Email,GioiTinh,NgaySinh,DiaChi) values('$username','$passwd','$pathName','$fullname','$mailAddress','$gender','$birthday','$address')";
			$executeInsert = mysqli_query($con,$queryInsert);
			if($executeInsert){
				echo"<script> alert('đăng ký thành công')</script>";
			}
			
		}

	 ?>
	<style>
		.red{
			color:red;
		}
	</style>
</body>
</html>