<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="../boostrap/css/bootstrap.min.css">
	<script type="text/javascript" src = "../boostrap/jquery-3.3.1.min.js"></script>
	<script type="text/javascript" src ="../boostrap/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<form action="#" method="POST">
					<h3 style="text-align: center;">
						Thông tin đặt tiệc
					</h3>
					<div class="form-group">
						<label for="#">Số khách tham gia</label>
						<input class="form-control" type="text" name="TotalCus">
					</div>
					<div class="form-group">
						<label for="#">Ngày </label>
						<input type="date" class="form-control" placeholder="dd/mm/yyyy" name="date">
					</div>
					<div class="form-group">
						<label for="#">Thời gian đặt tiệc</label>
						<input type="checkbox" name="time[]" value="Sang"> Sáng
						<input type="checkbox" name="time[]" value="Chieu"> Chiều
						<input type="checkbox" name="time[]" value="Toi"> Tối
					</div>
					<div class="form-group">
						<label for="#">Địa điểm</label>
						<input type="radio" name= "place" value ="Trong Nhà"> Trong nhà
						<input type="radio" name = "place" value="Ngoài trời"> Ngoài trời
					</div>
					<div class="form-group">
						<label for="#">Tên Khách Hàng</label>
						<input class="form-control" type="text" name="CustomerName">
					</div>
					<div class="form-group">
						<label for="#">Giới Tính</label>
						<input type="radio" name="gender" value="Nam">Nam
						<input type="radio" name="gender" value="Nữ">Nữ
					</div>
					<div class="form-group">
						<label for="#">Địa chỉ</label>
						<input type="text" class="form-control" name="address">
					</div>
					<div class="form-group">
						<label for="#">Yêu Cầu của khách hàng</label>
						<textarea class="form-control" cols="5" name="nes"></textarea>
					</div>
					<div class="form-group">
						<label for="#">khách hàng biết nhà hàng qua</label>
						<select name="youknowme">
							<option>Internet</option>
							<option>Friends</option>
							<option>News</option>
						</select>
					</div>
					<div style="text-align: center;">
						<button class="btn btn-primary" type="submit" name="OK">Đặt tiệc</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php 
		include 'connect.php';
		if(isset($_POST['OK'])){
			$soKhach = $_POST['TotalCus'];
			$date = $_POST['date'];
			$times = $_POST['time'];
			$place = $_POST['place'];
			$cusName = $_POST['CustomerName'];
			$gender = $_POST['gender'];
			$address = $_POST['address'];
			$nes = $_POST['nes'];
			$knowme = $_POST['youknowme'];
			$timeString = "";
			$percent = 0;
			foreach ($times as $value) {
				$timeString .=$value.' ';
				if($value == "Sang"){
					$percent += 10; 
				}
			};
			if($place == "Trong Nhà"){
				$percent +=5;
			}
			if($soKhach >20){
				$percent +=10;
			}
			$total  = $soKhach*200 - (($soKhach*200)*$percent/100);
			$query = "Insert into DatTiec(SoKhach,Ngay,ThoiGian,DiaDiem,TenKhach,GioiTinh,DiaChi,YeuCau,ThongTin,TongTien) values('$soKhach','$date','$timeString','$place','$cusName','$gender','$address','$nes','$knowme','$total')";
			$insertDb = mysqli_query($con,$query);
			if($insertDb){
				echo"<script> alert('thành công')</script>";
			}
		}
	 ?>
</body>
</html>