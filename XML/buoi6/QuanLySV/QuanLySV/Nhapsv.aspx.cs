﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanLySV
{
    public partial class Nhapsv : System.Web.UI.Page
    {
        public static string path = @"G:\Documents\thuchanhky5\XML\buoi6\QuanLySV\QuanLySV\XMLQLSV.xml";
        public void LoadData()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(path);
            GridView1.DataSource = ds.Tables[0];
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void Delete(object sender, GridViewDeleteEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            Sinhvien sv1 = new Sinhvien();
            sv1.Delete(ma, path);
            LoadData();
        }

        protected void Edit(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            LoadData();
        }

        protected void Update(object sender, GridViewUpdateEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            string ten = (GridView1.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text;
            string gt = (GridView1.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text;
            Sinhvien sv1 = new Sinhvien(ma, ten, gt);
            sv1.Update(sv1, path);
            LoadData();
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            Sinhvien sv1 = new Sinhvien(txtMasv.Text, txtTensv.Text, txtGioitinh.Text);
            sv1.Insert(sv1, path);
            LoadData();
        }
    }
}