﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace QuanLySV
{
    public partial class TraCuuDiem : System.Web.UI.Page
    {
        public static string path1 = @"G:\Documents\thuchanhky5\XML\buoi6\QuanLySV\QuanLySV\XMLQLSV.xml";
        public static string path2 = @"G:\Documents\thuchanhky5\XML\buoi6\QuanLySV\QuanLySV\XMLQLMH.xml";
        public static string path = @"G:\Documents\thuchanhky5\XML\buoi6\QuanLySV\QuanLySV\XMLQLD.xml";
       public void TimKiem()
        {
            XDocument xmlDoc1 = XDocument.Load(path1);
            XDocument xmlDoc2 = XDocument.Load(path2);
            XDocument xmlDoc3 = XDocument.Load(path);
            List<Sinhvien> sinhvien = (from q in xmlDoc1.Descendants("qlsv").Elements("Sv")
                                       select new Sinhvien
                                       {
                                           Masv = q.Element("Masv").Value,
                                           Hoten = q.Element("Hoten").Value,
                                           Gioitinh = q.Element("Gioitinh").Value,
                                       }).ToList();
            List<Diemsv> diem = (from q in xmlDoc3.Descendants("qld").Elements("Diem_SV")
                                       select new Diemsv
                                       {
                                           Masv = q.Element("Masv").Value,
                                           Mamon = q.Element("Mamon").Value,
                                           Diem = float.Parse(q.Element("Diem").Value)
                                       }).ToList();
            List<MonHoc> monhoc = (from q in xmlDoc2.Descendants("qlmh").Elements("Monhoc")
                                       select new MonHoc
                                       {
                                           Mamon = q.Element("Mamon").Value,
                                           Tenmon = q.Element("Tenmon").Value,
                                           SoTC = q.Element("Sotc").Value,
                                       }).ToList();
            var query = (from S in sinhvien
                         join d in diem on S.Masv equals d.Masv
                         join c in monhoc on d.Mamon equals c.Mamon
                         where S.Masv == txtmaso.Text
                         select new
                         {
                             S.Hoten,
                             d.Diem,
                             c.Mamon,
                             c.Tenmon,
                             c.SoTC
                         });
            DataTable dt = new DataTable();
            dt.Columns.Add("Mamon", typeof(string));
            dt.Columns.Add("Tenmon", typeof(string));
            dt.Columns.Add("Sotc", typeof(string));
            dt.Columns.Add("Diem", typeof(string));
            float DTB = 0;
            float tong1 = 0;
            int tong2 = 0;
            foreach (var item in query)
            {
                lblHoten.Text = "ho ten :" + item.Hoten;
                DataRow row;
                row = dt.NewRow(); row["Mamon"] = item.Mamon;
                row["Tenmon"] = item.Tenmon; row["Sotc"] = item.SoTC;
                row["Diem"] = item.Diem; row["Diem"] = item.Diem;
                tong2 = tong2 + Convert.ToInt32(item.SoTC);
                tong1 = tong1 + Convert.ToInt32(item.SoTC) * item.Diem;
                DTB = tong1 / tong2;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            TimKiem();
        }
    }
}