﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace QuanLySV
{
    public class Sinhvien
    {
        public string Masv { get; set; }
        public string Hoten { get; set; }
        public string Gioitinh { get; set; }

        public Sinhvien() { }
        public Sinhvien(string ma, string ten, string gt)
        {
            Masv = ma; Hoten = ten; Gioitinh = gt;
        }
        public void Update(Sinhvien sv1, string path)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            foreach (XElement x in xmlDoc1.Descendants("qlsv").Elements("Sv"))
            {
                if (x.Element("Masv").Value.ToString().Equals(sv1.Masv))
                {
                    x.Element("Hoten").Value = sv1.Hoten;
                    x.Element("Gioitinh").Value = sv1.Gioitinh;
                }
            }
            xmlDoc1.Save(path);
        }
        public void Delete(string ma, string path)
        {
            XmlDocument d = new XmlDocument();
            d.Load(path);
            XmlElement root = d.DocumentElement;
            XmlNodeList ls = root.ChildNodes;
            for (int i = 0; i < ls.Count; i++)
            {
                if (ls[i].ChildNodes[0].InnerText == ma)
                    root.RemoveChild(ls[i]);
            }
            d.Save(path);
        }

        public void Insert(Sinhvien sv1, string path)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            xmlDoc1.Element("qlsv").Add(
                new XElement("Sv",
                    new XElement("Masv", sv1.Masv),
                    new XElement("Hoten", sv1.Hoten),
                    new XElement("Gioitinh", sv1.Gioitinh)));
            xmlDoc1.Save(path);
        }
    }
}