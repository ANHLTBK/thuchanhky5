﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Nhapdiem.aspx.cs" Inherits="QuanLySV.Nhapdiem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 568px;
        }
        .auto-style3 {
            margin-left: 48px;
        }
        .auto-style4 {
            margin-left: 29px;
        }
        .auto-style5 {
            margin-left: 9px;
        }
        .auto-style6 {
            margin-left: 96px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" rowspan="4">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowDeleting="Delete" OnRowEditing="Edit" OnRowUpdating="Update" Width="567px" CellPadding="4" DataKeyNames="Masv,Mamon" ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Masv" HeaderText="Mã Sinh Viên" />
                                <asp:BoundField DataField="Mamon" HeaderText="Mã Môn" />
                                <asp:BoundField DataField="Diem" HeaderText="Điểm" />
                                <asp:ButtonField CommandName="Edit" HeaderText="Edit" ShowHeader="True" Text="Edit" />
                                <asp:ButtonField CommandName="Delete" HeaderText="Delete" ShowHeader="True" Text="Delete" />
                                <asp:ButtonField CommandName="Update" HeaderText="Update" ShowHeader="True" Text="Update" />
                            </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                        </asp:GridView>
                    </td>
                    <td>Mã Sinh Viên<asp:DropDownList ID="DropMaSV" runat="server" CssClass="auto-style5">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Mã Môn:
                        <asp:DropDownList ID="DropMamon" runat="server" CssClass="auto-style4">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>Điểm :
                        <asp:TextBox ID="txtDiem" runat="server" CssClass="auto-style3" Width="151px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnThem" runat="server" OnClick="btnThem_Click" Text="Insert" CssClass="auto-style6" Width="204px" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
