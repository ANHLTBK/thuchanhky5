﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace QuanLySV
{
    public class MonHoc
    {
        public string Mamon { get; set; }
        public string Tenmon { get; set; }
        public string SoTC { get; set; }
        public MonHoc() { }
        public MonHoc(string ma,string ten,string stc) {
            Mamon = ma; Tenmon = ten; SoTC = stc;
        }
    }
}