﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TraCuuDiem.aspx.cs" Inherits="QuanLySV.TraCuuDiem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 664px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">Tra Cứu Điểm</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">Nhập Mã Số<asp:TextBox ID="txtmaso" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblHoten" runat="server" Text="ho ten"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="667px">
                            <Columns>
                                <asp:BoundField DataField="Mamon" HeaderText="Mã Môn" />
                                <asp:BoundField DataField="Tenmon" HeaderText="Tên Môn" />
                                <asp:BoundField DataField="Sotc" HeaderText="Số Tín Chỉ" />
                                <asp:BoundField DataField="Diem" HeaderText="Điểm" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2">
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Tìm Kiếm" />
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
