﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace QuanLySV
{
    public partial class Nhapdiem : System.Web.UI.Page
    {
        public static string path1 = @"G:\Documents\thuchanhky5\XML\buoi6\QuanLySV\QuanLySV\XMLQLSV.xml";
        public static string path2 = @"G:\Documents\thuchanhky5\XML\buoi6\QuanLySV\QuanLySV\XMLQLMH.xml";
        public static string path = @"G:\Documents\thuchanhky5\XML\buoi6\QuanLySV\QuanLySV\XMLQLD.xml";
        public void LoadData()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(path);
            GridView1.DataSource = ds.Tables[0];
            GridView1.DataBind();
        }   
        protected void Page_Load(object sender, EventArgs e)
        {
            //Lấy mã Sinh Viên với path1
            XDocument xmlDoc1 = XDocument.Load(path1);
            DataTable dt = new DataTable();
            dt.Columns.Add("Masv", typeof(string));
            foreach (XElement x in xmlDoc1.Descendants("qlsv").Elements("Sv"))
            {
                DataRow row;
                row = dt.NewRow();
                row["Masv"] = x.Element("Masv").Value;
                dt.Rows.Add(row);
            }

            // Lấy Mã Môn
            XDocument xmlDoc2 = XDocument.Load(path2);
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Mamon", typeof(string));
            foreach (XElement x in xmlDoc2.Descendants("qlmh").Elements("Monhoc"))
            {
                DataRow row;
                row = dt1.NewRow();
                row["Mamon"] = x.Element("Mamon").Value;
                dt1.Rows.Add(row);
            }
            if (!IsPostBack)
            {
                LoadData();
                DropMaSV.DataSource = dt;
                DropMaSV.DataTextField = "Masv";
                DropMaSV.DataBind();

                DropMamon.DataSource = dt1;
                DropMamon.DataTextField = "Mamon";
                DropMamon.DataBind();
            }
        }

        protected void btnThem_Click(object sender, EventArgs e)
        {
            Diemsv d = new Diemsv(DropMaSV.Items[DropMaSV.SelectedIndex].Value,
                DropMamon.Items[DropMamon.SelectedIndex].Value,
              float.Parse(txtDiem.Text));
            d.Insert(d, path);
            LoadData();
        }

        protected void Delete(object sender, GridViewDeleteEventArgs e)
        {
            string masv = (GridView1.Rows[e.RowIndex].Cells[0].Controls[0] as TextBox).Text;
            string mamon = (GridView1.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text;
            Diemsv d = new Diemsv();
            d.Delete(masv, mamon, path);

            LoadData();
        }

        protected void Edit(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            LoadData();
        }

        protected void Update(object sender, GridViewUpdateEventArgs e)
        {
            string masv = (GridView1.Rows[e.RowIndex].Cells[0].Controls[0] as TextBox).Text;
            string mamon = (GridView1.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text;
            string diem = (GridView1.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text;

            Diemsv d = new Diemsv(masv, mamon, float.Parse(diem));
            d.Update(d, path);
            LoadData();
        }
    }
}