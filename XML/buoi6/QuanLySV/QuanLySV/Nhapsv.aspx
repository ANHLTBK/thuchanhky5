﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Nhapsv.aspx.cs" Inherits="QuanLySV.Nhapsv" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 572px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" rowspan="4">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" OnRowDeleting="Delete" OnRowEditing="Edit" OnRowUpdating="Update" CellPadding="4" DataKeyNames="Masv" ForeColor="#333333" GridLines="None" Width="567px">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:BoundField DataField="Masv" HeaderText="Mã Sinh Viên" />
                                <asp:BoundField DataField="Hoten" HeaderText="Tên Sinh Viên" />
                                <asp:BoundField DataField="Gioitinh" HeaderText="Giới Tính" />
                                <asp:ButtonField CommandName="Edit" HeaderText="Edit" ShowHeader="True" Text="Edit" />
                                <asp:ButtonField CommandName="Update" HeaderText="Update" ShowHeader="True" Text="Update" />
                                <asp:ButtonField CommandName="Delete" HeaderText="Delete" ShowHeader="True" Text="Delete" />
                            </Columns>
                            <EditRowStyle BackColor="#7C6F57" />
                            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#E3EAEB" />
                            <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F8FAFA" />
                            <SortedAscendingHeaderStyle BackColor="#246B61" />
                            <SortedDescendingCellStyle BackColor="#D4DFE1" />
                            <SortedDescendingHeaderStyle BackColor="#15524A" />
                        </asp:GridView>
                    </td>
                    <td>Mã Sinh Viên :<asp:TextBox ID="txtMasv" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Họ Tên :<asp:TextBox ID="txtTensv" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Giới Tính:
                        <asp:TextBox ID="txtGioitinh" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnInsert" runat="server" OnClick="btnInsert_Click" style="height: 26px" Text="Insert" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
