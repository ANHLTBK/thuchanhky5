﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;

namespace QuanLySV
{
    public class Diemsv
    {
        public string Masv { get; set; }
        public string Mamon { get; set; }
        public float Diem { get; set; }

        public Diemsv() { }
        public Diemsv(string ma, string ma1, float dm)
        {
            Masv = ma; Mamon = ma1; Diem = dm;
        }
        public void Update(Diemsv dm1, string path)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            foreach (XElement x in xmlDoc1.Descendants("qld").Elements("Diem_SV"))
            {
                if ((x.Element("Masv").Value.ToString().Equals(dm1.Masv)) &&
                    (x.Element("Mamon").Value.ToString().Equals(dm1.Mamon)))
                {
                    x.Element("Diem").Value = dm1.Diem.ToString();
                }
            }
            xmlDoc1.Save(path);
        }
        public void Delete(string ma, string ma1, string path)
        {
            XmlDocument d = new XmlDocument();
            d.Load(path);
            XmlElement root = d.DocumentElement;
            XmlNodeList ls = root.ChildNodes;
            for (int i = 0; i < ls.Count; i++)
            {
                if ((ls[i].ChildNodes[0].InnerText == ma) && (ls[i].ChildNodes[1].InnerText == ma1))
                    root.RemoveChild(ls[i]);
            }
            d.Save(path);
        }

        public void Insert(Diemsv dm1, string path)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            xmlDoc1.Element("qld").Add(
                new XElement("Diem_SV",
                    new XElement("Masv", dm1.Masv),
                    new XElement("Mamon", dm1.Mamon),
                    new XElement("Diem", dm1.Diem)));
            xmlDoc1.Save(path);
        }
    }
}