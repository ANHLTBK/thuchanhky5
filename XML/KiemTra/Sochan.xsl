<?xml version="1.0"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/REC-html40">
	
	<xsl:output method="text" />
	<xsl:template match="/" >
	
	Cac so chan: 
		<xsl:if test="/Goc/So[1]/@a mod 2 = 0">
			<xsl:value-of select="/Goc/So[1]/@a" />
		</xsl:if> ,
		
		<xsl:if test="/Goc/So[2]/@a mod 2 = 0">
			<xsl:value-of select="/Goc/So[2]/@a" />
		</xsl:if>
		
		<xsl:if test="/Goc/So[3]/@a mod 2 = 0">
			<xsl:value-of select="/Goc/So[3]/@a" />
		</xsl:if>
			
	</xsl:template>

</xsl:stylesheet>
