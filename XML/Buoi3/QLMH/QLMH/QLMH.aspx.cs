﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace QLMH
{
    public partial class QLMH : System.Web.UI.Page
    {
        string path = "G:/Documents/thuchanhky5/XML/Buoi3MonHoc.xml";
        public void LoadData()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(path);
            GridView1.DataSource = ds.Tables[0];
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {   
            try
            {
                if (!IsPostBack)
                {
                    LoadData();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Delete(object sender, GridViewDeleteEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            XmlDocument d = new XmlDocument();
            d.Load(path);
            XmlElement root = d.DocumentElement;
            XmlNodeList ls = root.ChildNodes;
            for (int i = 0; i < ls.Count; i++)
            {
                if (ls[i].ChildNodes[0].InnerText == ma)
                {
                    root.RemoveChild(ls[i]);
                }
                d.Save(path);
                LoadData();
            }
        }

        protected void Edit(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            LoadData();
        }

        protected void Updata(object sender, GridViewUpdateEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            string tensv = (GridView1.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text;
            string lop = (GridView1.Rows[e.RowIndex].Cells[2].Controls[1] as TextBox).Text;

            XmlDocument root = new XmlDocument();
            root.Load(path);
            XmlNodeList ls = root.GetElementsByTagName("student");
            foreach (XmlNode n in ls)
            {
                if (n.ChildNodes[0].InnerText == ma)
                {
                    n.ChildNodes[1].InnerText = tensv;
                    n.ChildNodes[2].InnerText = lop;
                }
                root.Save(path);
            }
            LoadData();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}