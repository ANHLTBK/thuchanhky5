﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace QLMH
{
    public partial class Diem : System.Web.UI.Page
    {
        string path = @"G:/Documents/thuchanhky5/XML/Buoi3MonHoc.xml";
        public void LoadData()
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            string xpath = "//student[@code='" + txtma.Text + "']";
            XmlNode node = xmlDoc.SelectSingleNode(xpath);
            lblten.Text = "Ho va Ten:" + node.Attributes[1].InnerText;
            DataTable dt = new DataTable();
            dt.Columns.Add("code", typeof(string));
            dt.Columns.Add("name", typeof(string));
            dt.Columns.Add("point", typeof(string));
            for(int i=0; i < node.ChildNodes.Count; i++)
            {
                string code = node.ChildNodes[i].Attributes[0].InnerText;
                string name = node.ChildNodes[i].Attributes[1].InnerText;
                string diem = node.ChildNodes[i].Attributes[2].InnerText;
                DataRow row;
                row = dt.NewRow();
                row["code"] = code;
                row["name"] = name;
                row["point"] = diem;
                dt.Rows.Add(row);
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Delete(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void Edit(object sender, GridViewEditEventArgs e)
        {

        }

        protected void Updata(object sender, GridViewUpdateEventArgs e)
        {

        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
                LoadData();
        }
    }
}