﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Xml.Linq;

namespace DanhSachKH
{
    
    public partial class DanhSachKH : System.Web.UI.Page
    {
        string path = "D:\\XML\\TH6_2.xml";
        public void LoadData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("MaKH", typeof(string));
            dt.Columns.Add("TenKH", typeof(string));
            dt.Columns.Add("MaMH", typeof(string));
            dt.Columns.Add("TenMH", typeof(string));
            dt.Columns.Add("DonGia", typeof(string));
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList danhsach = xmlDoc.GetElementsByTagName("khachhang");
            for (int i = 0; i < danhsach.Count; i++)
            {
                string makh = danhsach[i].Attributes[0].InnerText;
                string tenkh = danhsach[i].Attributes[0].InnerText;
                for (int j = 0; j < danhsach[i].ChildNodes.Count; j++)
                {
                    string mamh = danhsach[i].ChildNodes[j].Attributes[0].InnerText;
                    string tenmh = danhsach[i].ChildNodes[j].Attributes[1].InnerText;
                    string dg = danhsach[i].ChildNodes[j].Attributes[2].InnerText;                 DataRow row;
                    row = dt.NewRow();
                    row["MaKH"] = makh;
                    row["TenKH"] = tenkh;
                    row["MaMH"] = mamh;
                    row["TenMH"] = tenmh;
                    row["DonGia"] = dg;
                    dt.Rows.Add(row);
                }
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnLoc_Click(object sender, EventArgs e)
        {
            string xpath = "//khachhang[@code='" + txtMa.Text + "']";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList donhang = xmlDoc.SelectNodes(xpath);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaKH", typeof(string));
            dt.Columns.Add("TenKH", typeof(string));
            dt.Columns.Add("MaMH", typeof(string));
            dt.Columns.Add("TenMH", typeof(string));
            dt.Columns.Add("DonGia", typeof(string));
            for(int i=0; i < donhang.Count; i++)
            {
                string makh = donhang[i].Attributes[0].InnerText;
                string tenkh = donhang[i].Attributes[1].InnerText;
                for(int j = 0; j < donhang[i].ChildNodes.Count; j++)
                {
                    string mamh = donhang[i].ChildNodes[j].Attributes[0].InnerText;
                    string tenmh = donhang[i].ChildNodes[j].Attributes[1].InnerText;
                    string gia = donhang[i].ChildNodes[j].Attributes[2].InnerText;
                    DataRow row;
                    row = dt.NewRow();
                    row["MaKH"] = makh;
                    row["TenKH"] = tenkh;
                    row["MaMH"] = mamh;
                    row["TenMH"] = tenmh;
                    row["DonGia"] = gia;
                    dt.Rows.Add(row);
                }
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}