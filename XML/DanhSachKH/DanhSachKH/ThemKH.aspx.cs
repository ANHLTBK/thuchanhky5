﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using System.Data;

namespace DanhSachKH
{
    public partial class ThemKH : System.Web.UI.Page
    {
        string path = "D:\\XML\\TH6_2.xml";
        protected void Page_Load(object sender, EventArgs e)
        {
            XDocument xDoc1 = XDocument.Load(path);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaKH", typeof(string));
            dt.Columns.Add("TenKH", typeof(string));
            foreach(XElement x in xDoc1.Descendants("QLKH").Elements("MaKH"))
            {
                DataRow row;
                row = dt.NewRow();
                row["MaKH"] = x.Attribute("code").Value;
                row["TenKH"] = x.Attribute("name").Value;
                dt.Rows.Add(row);
            }
        }


        protected void btnThemKH_Click(object sender, EventArgs e)
        {

        }
    }
}