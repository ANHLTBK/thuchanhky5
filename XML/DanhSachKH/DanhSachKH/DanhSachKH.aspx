﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DanhSachKH.aspx.cs" Inherits="DanhSachKH.DanhSachKH" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 399px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td>DANH SÁCH KHÁCH HÀNG</td>
                </tr>
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style2">
                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                                        <Columns>
                                            <asp:BoundField DataField="MaKH" HeaderText="Mã Khách Hàng" />
                                            <asp:BoundField DataField="TenKH" HeaderText="Tên Khách Hàng" />
                                            <asp:BoundField DataField="MaMH" HeaderText="Mã Mặt Hàng" />
                                            <asp:BoundField DataField="TenMH" HeaderText="Tên Mặt Hàng" />
                                            <asp:BoundField DataField="DonGia" HeaderText="Đơn giá" />
                                        </Columns>
                                    </asp:GridView>
                                </td>
                                <td>
                                    <table class="auto-style1">
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td>Nhập Mã:
                                                <asp:TextBox ID="txtMa" runat="server"></asp:TextBox>
                                                <asp:Button ID="btnLoc" runat="server" OnClick="btnLoc_Click" Text="Lọc" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
