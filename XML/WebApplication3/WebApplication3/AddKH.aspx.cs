﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;

namespace WebApplication3
{
    public partial class AddKH : System.Web.UI.Page
    {
        string path = "G:\\Documents\\thuchanhky5\\XML\\QLJH.xml";
        protected void Page_Load(object sender, EventArgs e)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaKH", typeof(string));
            foreach (XElement x in xmlDoc1.Descendants("QLKH").Elements("MaKH"))
            {
                DataRow row;
                row = dt.NewRow();
                row["MaKH"] = x.Attribute("id").Value;
                dt.Rows.Add(row);
            }
        }

        protected void btnThemKH_Click(object sender, EventArgs e)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            xmlDoc1.Element("QLKH").Add(
                                        new XElement("MaLop",
                                        new XAttribute("id", txtMaMH.Text)),
                                        new XElement("TenKH",
                                        new XAttribute("id", txtTenKH.Text)));
            xmlDoc1.Save("G:\\Documents\\thuchanhky5\\XML\\QLJH.xml");
        }

        protected void btnThemMH_Click(object sender, EventArgs e)
        {
            //XDocument xmlDoc1 = XDocument.Load(path);
            //foreach (XElement x in xmlDoc1.Descendants("QLSV").Elements("MaLop"))
            //{
            //    if (x.Attribute("id").Value.ToString() == cboMalop.Items[cboMalop.SelectedIndex].Value)
            //    {
            //        x.Add(
            //                new XElement("SV",
            //                new XElement("Masv", txtMasv.Text),
            //                new XElement("Tensv", txtHoten.Text),
            //                new XElement("Gioitinh", txtGioitinh.Text),
            //                new XElement("DTB", txtDtb.Text)));
            //    }
            //}
            //xmlDoc1.Save("G:\\Documents\\thuchanhky5\\XML\\QLJH.xml");
        }
    }
}