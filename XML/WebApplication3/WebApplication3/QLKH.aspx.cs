﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace WebApplication3
{
    public partial class QLKH : System.Web.UI.Page
    {
        string path = "G:\\Documents\\thuchanhky5\\XML\\QLJH.xml";
        public void LoadData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("MaKH", typeof(string));
            dt.Columns.Add("TenKH", typeof(string));
            dt.Columns.Add("MaMH", typeof(string));
            dt.Columns.Add("TenMH", typeof(string));
            dt.Columns.Add("DonGia", typeof(string));
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList danhsach = xmlDoc.GetElementsByTagName("khachhang");
            for (int i = 0; i < danhsach.Count; i++)
            {
                string makh = danhsach[i].Attributes[0].InnerText;
                string tenkh = danhsach[i].Attributes[0].InnerText;
                for (int j = 0; j < danhsach[i].ChildNodes.Count; j++)
                {
                    string mamh = danhsach[i].ChildNodes[j].Attributes[0].InnerText;
                    string tenmh = danhsach[i].ChildNodes[j].Attributes[1].InnerText;
                    string dg = danhsach[i].ChildNodes[j].Attributes[2].InnerText;
                    DataRow row;
                    row = dt.NewRow();
                    row["MaKH"] = makh;
                    row["TenKH"] = tenkh;
                    row["MaMH"] = mamh;
                    row["TenMH"] = tenmh;
                    row["DonGia"] = dg;
                    dt.Rows.Add(row);
                }
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnLoc_Click(object sender, EventArgs e)
        {
            string xpath = "//khachhang[@MaKH='" + txtMa.Text + "']";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList donhang = xmlDoc.SelectNodes(xpath);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaKH", typeof(string));
            dt.Columns.Add("TenKH", typeof(string));
            dt.Columns.Add("MaMH", typeof(string));
            dt.Columns.Add("TenMH", typeof(string));
            dt.Columns.Add("DonGia", typeof(string));
            for (int i = 0; i < donhang.Count; i++)
            {
                string makh = donhang[i].Attributes[0].InnerText;
                string tenkh = donhang[i].Attributes[1].InnerText;
                for (int j = 0; j < donhang[i].ChildNodes.Count; j++)
                {
                    string mamh = donhang[i].ChildNodes[j].Attributes[0].InnerText;
                    string tenmh = donhang[i].ChildNodes[j].Attributes[1].InnerText;
                    string gia = donhang[i].ChildNodes[j].Attributes[2].InnerText;
                    DataRow row;
                    row = dt.NewRow();
                    row["MaKH"] = makh;
                    row["TenKH"] = tenkh;
                    row["MaMH"] = mamh;
                    row["TenMH"] = tenmh;
                    row["DonGia"] = gia;
                    dt.Rows.Add(row);
                }
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnLocGia_Click(object sender, EventArgs e)
        {
            string xpath = "//khachhang//mathang[@DonGia>50000]";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList node = xmlDoc.SelectNodes(xpath);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaMH", typeof(string));
            dt.Columns.Add("TenMH", typeof(string));
            dt.Columns.Add("MaKH", typeof(string));
            dt.Columns.Add("TenKH", typeof(string));
            dt.Columns.Add("DonGia", typeof(string));
            GridView1.Columns[0].Visible = false;// an cot ma lop
            foreach (XmlNode n in node)
            {
                string tenmh = n.Attributes[0].InnerText;
                string mamh = n.Attributes[1].InnerText;
                string dongia = n.Attributes[2].InnerText;
                DataRow row;
                row = dt.NewRow();
                row["MaMH"] = mamh;
                row["TenMH"] = tenmh;
                row["DonGia"] = dongia;
                dt.Rows.Add(row);
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}