﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="QLKH.aspx.cs" Inherits="WebApplication3.QLKH" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 405px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" rowspan="2">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="MaKH" HeaderText="Ma KH" />
                                <asp:BoundField DataField="TenKH" HeaderText="Ten KH" />
                                <asp:BoundField DataField="TenMH" HeaderText="Ten San Pham" />
                                <asp:BoundField DataField="MaMH" HeaderText="Ma SP" />
                                <asp:BoundField DataField="DonGia" HeaderText="Don Gia" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>nhap ma:
                        <asp:TextBox ID="txtMa" runat="server"></asp:TextBox>
                        <asp:Button ID="btnLocGia" runat="server" OnClick="btnLocGia_Click" Text="gia don hang &gt;50000" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnLoc" runat="server" OnClick="btnLoc_Click" Text="Loc" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
