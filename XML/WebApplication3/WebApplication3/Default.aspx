﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApplication3.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 395px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style2" rowspan="3">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundField DataField="Masv" HeaderText="Ma SinhVien" />
                                <asp:BoundField DataField="MaLop" HeaderText="Ma Lop" />
                                <asp:BoundField DataField="Tensv" HeaderText="Ten SinhVien" />
                                <asp:BoundField DataField="Gioitinh" HeaderText="gioi Tinh" />
                                <asp:BoundField DataField="DTB" HeaderText="Diem tb" />
                            </Columns>
                        </asp:GridView>
                    </td>
                    <td>Nhap ma lop</td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtMaLop" runat="server"></asp:TextBox>
                        <asp:Button ID="btnDTB" runat="server" OnClick="btnDTB_Click" Text="Sinh viên có ĐTB &gt; 5" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="bntTimKiem" runat="server" Text="Tim kiem" OnClick="bntTimKiem_Click" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
