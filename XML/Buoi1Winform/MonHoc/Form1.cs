﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MonHoc.DTO;
using MonHoc.BLL;

namespace MonHoc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        MonHocBLL monhocBLL = new MonHocBLL();
        MonHocDTO monhocDTO = new MonHocDTO();

        private void btnThem_Click(object sender, EventArgs e)
        {
                if (txtMaMon.Text.Trim() != "")
            {
                //gán dữ liệu vào DTO
                monhocDTO.MAMON = txtMaMon.Text.ToLower();
                monhocDTO.TENMON = txtTenMon.Text;
                monhocDTO.SOTC = int.Parse(txtSoTinChi.Text);
                //gọi BLL thực hiện
                monhocBLL.Them(monhocDTO);
                //hiện lên dgv
                monhocBLL.HienThi(dgv);
            }
        }

        private void btnNhap_Click(object sender, EventArgs e)
        {
            monhocBLL.HienThi(dgv);
        }

        private void btnSua_Click(object sender, EventArgs e)
        {
            if (txtMaMon.Text.Trim() != "")
            {
                //gán dữ liệu vào DTO
                monhocDTO.MAMON = txtMaMon.Text.ToLower();
                monhocDTO.TENMON = txtTenMon.Text;
                monhocDTO.SOTC = int.Parse(txtSoTinChi.Text);
                //gọi BLL thực hiện
                monhocBLL.Sua(monhocDTO);
                //hiện lên dgv
                monhocBLL.HienThi(dgv);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (txtMaMon.Text.Trim() != "")
            {
                //gán dữ liệu vào DTO
                monhocDTO.MAMON = txtMaMon.Text.ToLower();

                //gọi BLL thực hiện
                monhocBLL.Xoa(monhocDTO);
                //hiện lên dgv
                monhocBLL.HienThi(dgv);
            }
        }
    }
}
