﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MonHoc.DTO;
using System.Windows.Forms;
using System.Xml;

namespace MonHoc.BLL
{
    public class MonHocBLL
    {
        XmlDocument doc = new XmlDocument();
        XmlElement root;
        string fileName = @"G:\Documents\thuchanhky5\XML\fileXML.xml";
        public MonHocBLL()
        {
            doc.Load(fileName);
            root = doc.DocumentElement;
        }
        public void Them(MonHocDTO sachThem)
        {
            //tạo nút sách
            XmlNode sach = doc.CreateElement("MH");

            //tạo nút con của sách là masach
            XmlElement masach = doc.CreateElement("MaMon");
            masach.InnerText = sachThem.MAMON;//gán giá trị cho mã sách
            sach.AppendChild(masach);//thêm masach vào trong sách(sach nhận masach là con)

            XmlElement tensach = doc.CreateElement("TenMon");
            tensach.InnerText = sachThem.TENMON;
            sach.AppendChild(tensach);

            XmlElement soluong = doc.CreateElement("SoTinChi");
            soluong.InnerText = sachThem.SOTC.ToString();
            sach.AppendChild(soluong);

            root.AppendChild(sach);
            doc.Save(fileName);

        }
        public void Sua(MonHocDTO sachSua)
        {
            //láy vị trí cần sửa theo mã sách cũ đưa vào
            XmlNode sachCu = root.SelectSingleNode("MH[MaMon ='" + sachSua.MAMON + "']");
            if (sachCu != null)
            {
                XmlNode sachSuaMoi = doc.CreateElement("MH");

                //tạo nút con của sách là masach
                XmlElement masach = doc.CreateElement("MaMon");
                masach.InnerText = sachSua.MAMON;//gán giá trị cho mã sách
                sachSuaMoi.AppendChild(masach);//thêm masach vào trong sách(sach nhận masach là con)

                XmlElement tensach = doc.CreateElement("TenMon");
                tensach.InnerText = sachSua.TENMON;
                sachSuaMoi.AppendChild(tensach);

                XmlElement soluong = doc.CreateElement("SoTinChi");
                soluong.InnerText = sachSua.SOTC.ToString();
                sachSuaMoi.AppendChild(soluong);
                root.ReplaceChild(sachSuaMoi, sachCu);
                doc.Save(fileName);
            }
        }
        public void Xoa(MonHocDTO sachXoa)
        {
            XmlNode sachCanXoa = root.SelectSingleNode("MH[MaMon ='" + sachXoa.MAMON + "']");
            if (sachCanXoa != null)
            {
                root.RemoveChild(sachCanXoa);

                doc.Save(fileName);
            }
        }

        public void HienThi(DataGridView dgv)
        {
            dgv.Rows.Clear();
            dgv.ColumnCount = 3;

            XmlNodeList ds = root.SelectNodes("MH");
            int sd = 0;//lưu chỉ số dòng
            foreach (XmlNode item in ds)
            {
                dgv.Rows.Add();
                dgv.Rows[sd].Cells[0].Value = item.SelectSingleNode("MaMon").InnerText;
                dgv.Rows[sd].Cells[1].Value = item.SelectSingleNode("TenMon").InnerText;
                dgv.Rows[sd].Cells[2].Value = item.SelectSingleNode("SoTinChi").InnerText;
                sd++;
            }
        }
    }
}
