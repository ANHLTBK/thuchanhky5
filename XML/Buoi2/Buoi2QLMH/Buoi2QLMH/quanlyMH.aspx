﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="quanlyMH.aspx.cs" Inherits="Buoi2QLMH.quanlyMH" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 564px;
        }
        .auto-style4 {
            margin-left: 17px;
        }
        .auto-style5 {
            margin-left: 26px;
        }
        .auto-style6 {
            margin-left: 24px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style2" colspan="2">THÔNG TIN VỀ MÔN HỌC</td>
            </tr>
            <tr>
                <td class="auto-style3" rowspan="4">
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="Mamon" ForeColor="#333333" GridLines="None" OnRowDeleting="Delete" OnRowEditing="Edit" OnRowUpdating="Update" Width="562px">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="Mamon" HeaderText="Mã Môn" />
                            <asp:BoundField DataField="TenMon" HeaderText="Tên Môn" />
                            <asp:BoundField DataField="SoTC" HeaderText="Số Tín Chỉ" />
                            <asp:ButtonField CommandName="Select" Text="Select" />
                            <asp:ButtonField CommandName="Delete" Text="Delecte" />
                            <asp:ButtonField CommandName="Update" Text="Update" />
                            <asp:ButtonField CommandName="Edit" Text="Edit" />
                        </Columns>
                        <EditRowStyle BackColor="#7C6F57" />
                        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#E3EAEB" />
                        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F8FAFA" />
                        <SortedAscendingHeaderStyle BackColor="#246B61" />
                        <SortedDescendingCellStyle BackColor="#D4DFE1" />
                        <SortedDescendingHeaderStyle BackColor="#15524A" />
                    </asp:GridView>
                </td>
                <td>Mã Môn : <asp:TextBox ID="txtMamon" runat="server" CssClass="auto-style6" Height="16px" Width="245px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Tên Môn :<asp:TextBox ID="txtTenmon" runat="server" CssClass="auto-style5" Height="16px" Width="245px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Số Tín Chỉ :<asp:TextBox ID="txtSoTC" runat="server" CssClass="auto-style4" Height="16px" Width="245px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Insert" Width="184px" />
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
