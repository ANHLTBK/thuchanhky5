﻿using System;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Buoi2QLMH
{
    public partial class quanlyMH : System.Web.UI.Page
    {
        string path = "G:/Documents/thuchanhky5/XML/fileXML.xml";
        public void LoadData()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(path);
            GridView1.DataSource = ds.Tables[0];
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try {
                if (!IsPostBack)
                {
                    LoadData();
                }
            }
            catch(Exception ex)
            {

            }
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Delete(object sender, GridViewDeleteEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            XmlDocument d = new XmlDocument();
            d.Load(path);
            XmlElement root = d.DocumentElement;
            XmlNodeList ls = root.ChildNodes;
            for(int i = 0; i < ls.Count; i++)
            {
                if(ls[i].ChildNodes[0].InnerText == ma)
                {
                    root.RemoveChild(ls[i]);
                }
                d.Save(path);
                LoadData();
            }
        }

        protected void Edit(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            LoadData();
        }
        protected void Update(object sender, GridViewUpdateEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            string tenmon = (GridView1.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text;
            string SoTC = (GridView1.Rows[e.RowIndex].Cells[2].Controls[1] as TextBox).Text;

            XmlDocument root = new XmlDocument();
            root.Load(path);
            XmlNodeList ls = root.GetElementsByTagName("Monhoc");
            foreach(XmlNode n in ls)
            {
                if(n.ChildNodes[0].InnerText == ma){
                    n.ChildNodes[1].InnerText = tenmon;
                    n.ChildNodes[2].InnerText = SoTC;
                }
                root.Save(path);
            }
            LoadData();

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
           
            XDocument root = XDocument.Load(path);
           
            XElement newmh = new XElement("Monhoc",
                new XElement("Mamon", txtMamon.Text),
                new XElement("Tenmon", txtTenmon.Text),
                new XElement("SoTC", txtSoTC.Text)
                );
            root.Element("QUANlyMH").Add(newmh);
            root.Save(path);
            LoadData();

        }
    }
}