<?xml version="1.0"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/REC-html40">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:value-of select="/GOC/SO[1]/@Gia_Tri"/>+ <xsl:value-of select="/GOC/SO[2]/@Gia_Tri"/>=
		<xsl:value-of select="/GOC/SO[1]/@Gia_Tri+/GOC/SO[2]/@Gia_Tri"/>
	</xsl:template>
</xsl:stylesheet>
