<?xml version="1.0"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/REC-html40">
	<xsl:output method="html"/>
	<xsl:template match="/">
		<html>
			<body>
				<xsl:apply-templates />
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="TRUONG">
		<ul>
			<xsl:value-of select="@Ten" />
			<xsl:apply-templates />
		</ul>
	</xsl:template>
	
	<xsl:template match="KHOI">
		<ul>
			<xsl:value-of select="@Ten" />
			<xsl:apply-templates />
		</ul>
	</xsl:template>
	
	<xsl:template match="LOP">
		<ul>
			<xsl:value-of select="@Ten" />
			<xsl:apply-templates />
		</ul>
	</xsl:template>
</xsl:stylesheet>
