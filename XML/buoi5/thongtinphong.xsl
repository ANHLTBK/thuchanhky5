<?xml version="1.0"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/REC-html40">
	<xsl:output method="html"/>
	<xsl:template match="/" >
	
	<HTML>
		<BODY>
			<TABLE >
				<TR>
					<TD colspan="2">Bang gia thue phong</TD>
				</TR>
				<TR>
					<TD colspan="2">Loai phong don gia</TD>
				</TR>
				
				<xsl:for-each select="/KHACH_SAN/LOAI_PHONG">
				<xsl:sort select="@Don_Gia"   data-type="number" order="descending" />
				<TR>
					<TD><xsl:value-of select="@Ten" /></TD>
					<TD><xsl:value-of select="@Don_Gia" /></TD>
				</TR>
				</xsl:for-each>
			</TABLE>
		</BODY>
	</HTML>
	</xsl:template>
</xsl:stylesheet>
