<?xml version="1.0"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/REC-html40">
	<xsl:output method="html"/>
	<xsl:template match="/" >
	
		<HTML>
		<BODY>
			<TABLE border="1">
				
				<TR>
					<TD>Ma</TD>
					<TD>Ten</TD>
					<TD>Mo ta</TD>
					<TD>Gia</TD>
					<TD>So luong</TD>
				</TR>
				<xsl:for-each select="/PRODUCTDATA/PRODUCT">
				<xsl:sort select="@SOLUONG"  data-type="number" order="ascending" />
				<TR>
					<TD><xsl:value-of select="@MA" /></TD>
					<TD><xsl:value-of select="TEN" /></TD>
					<TD><xsl:value-of select="MOTA" /></TD>
					<TD><xsl:value-of select="GIA" /></TD>
					<TD><xsl:value-of select="SOLUONG" /></TD>
				</TR>
				
				
				</xsl:for-each>
			</TABLE>
		</BODY>
	</HTML>
	</xsl:template>

</xsl:stylesheet>
