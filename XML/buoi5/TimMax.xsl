<?xml version="1.0"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/REC-html40">
	<xsl:output method="text"/>
	<xsl:template match="/">
		<xsl:if test="/MAX/SO[1]/@Gia_Tri &gt; /MAX/SO[2]/@Gia_Tri">	
			<xsl:if test="/MAX/SO[1]/@Gia_Tri &gt; /MAX/SO[3]/@Gia_Tri">
			so lon nhat la: <xsl:value-of select="/MAX/SO[1]/@Gia_Tri"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="/MAX/SO[2]/@Gia_Tri &gt; /MAX/SO[1]/@Gia_Tri">	
			<xsl:if test="/MAX/SO[2]/@Gia_Tri &gt; /MAX/SO[3]/@Gia_Tri">
			so lon nhat la: <xsl:value-of select="/MAX/SO[2]/@Gia_Tri"/>
			</xsl:if>
		</xsl:if>
		<xsl:if test="/MAX/SO[3]/@Gia_Tri &gt; /MAX/SO[2]/@Gia_Tri">	
			<xsl:if test="/MAX/SO[3]/@Gia_Tri &gt; /MAX/SO[1]/@Gia_Tri">
			so lon nhat la: <xsl:value-of select="/MAX/SO[3]/@Gia_Tri"/>
			</xsl:if>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
