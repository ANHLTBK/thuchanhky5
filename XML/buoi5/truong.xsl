<?xml version="1.0"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.w3.org/TR/REC-html40">
	<xsl:output method="html"/>
	<xsl:template match="/" >
	
		<HTML>
		<BODY>
			<TABLE border="1">
				
				<TR>
					<TD>Khoi</TD>
					<TD>Lop</TD>
				</TR>
				<xsl:for-each select="/TRUONG/KHOI">
				<TR>
					<TD><xsl:value-of select="@MA" /></TD>
					<TD><xsl:value-of select="LOP" /></TD>
				</TR>
				
				
				</xsl:for-each>
			</TABLE>
		</BODY>
	</HTML>
	</xsl:template>
</xsl:stylesheet>
