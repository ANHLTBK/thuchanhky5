﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="qlynv.aspx.cs" Inherits="QuanlyNV.qlynv" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 506px;
        }
        .auto-style4 {
            margin-left: 2px;
        }
        .auto-style5 {
            height: 24px;
        }
        .auto-style6 {
            height: 31px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2" colspan="2">Quan Ly Nhan Vien</td>
                </tr>
                <tr>
                    <td class="auto-style3" rowspan="5">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="MaNV" ForeColor="#333333" GridLines="None" OnRowDeleting="Delete" OnRowEditing="Edit" OnRowUpdating="UpDate" Width="505px">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundField HeaderText="Mã Nhân Viên" />
                                <asp:BoundField HeaderText="Tên Nhân Viên" />
                                <asp:BoundField HeaderText="Công Việc" />
                                <asp:BoundField HeaderText="Quốc Gia" />
                                <asp:ButtonField CommandName="Cancel" Text="Delete" />
                                <asp:ButtonField CommandName="Cancel" Text="UpDate" />
                                <asp:ButtonField CommandName="Cancel" Text="Edit" />
                                <asp:ButtonField CommandName="Cancel" Text="Select" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                        </asp:GridView>
                    </td>
                    <td>Mã Nhân Viên :<asp:TextBox ID="txtMaNV" runat="server" CssClass="auto-style4" Width="183px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>Tên Nhân Viên :<asp:TextBox ID="txtTenNV" runat="server" CssClass="auto-style4" Width="183px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">Công Việc :<asp:TextBox ID="txtCV" runat="server" CssClass="auto-style4" Width="183px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">Quốc Gia :<asp:TextBox ID="TextBox4" runat="server" CssClass="auto-style4" Width="183px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Insert" Width="165px" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
