﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace QuanlyNV
{
    public partial class qlynv : System.Web.UI.Page
    {
        string path = "G:/Documents/thuchanhky5/XML/QLNV.xml";
        public void LoadData()
        {
            DataSet ds = new DataSet();
            ds.ReadXml(path);
            GridView1.DataSource = ds.Tables[0]; ;
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadData();
                }
            }
            catch (Exception ex)
            {

            }
        }

        protected void Delete(object sender, GridViewDeleteEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            XmlDocument d = new XmlDocument();
            d.Load(path);
            XmlElement root = d.DocumentElement;
            XmlNodeList ls = root.ChildNodes;
            for(int i = 0; i < ls.Count; i++)
            {
                root.RemoveChild(ls[i]);
            }
            d.Save(path);
            LoadData();

        }

        protected void Edit(object sender, GridViewEditEventArgs e)
        {
            GridView1.EditIndex = e.NewEditIndex;
            LoadData();
        }

        protected void UpDate(object sender, GridViewUpdateEventArgs e)
        {
            string ma = GridView1.DataKeys[e.RowIndex].Value.ToString();
            string ten = (GridView1.Rows[e.RowIndex].Cells[1].Controls[0] as TextBox).Text;
            string cv = (GridView1.Rows[e.RowIndex].Cells[2].Controls[0] as TextBox).Text;
            string qg = (GridView1.Rows[e.RowIndex].Cells[3].Controls[0] as TextBox).Text;

            XmlDocument root = new XmlDocument();
            root.Load(path);
            XmlNodeList ls = root.GetElementsByTagName("NV");
            foreach (XmlNode n in ls)
            {
                if (n.ChildNodes[0].InnerText == ma)
                {
                    n.ChildNodes[1].InnerText = ten;
                    n.ChildNodes[2].InnerText = cv;
                    n.ChildNodes[2].InnerText = qg;
                }
                root.Save(path);
            }
            LoadData();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
    }
}