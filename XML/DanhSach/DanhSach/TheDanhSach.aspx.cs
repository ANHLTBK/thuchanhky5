﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;
using System.Xml.Linq;

namespace DanhSach
{
    public partial class TheDanhSach : System.Web.UI.Page
    {
        string path = "D:\\XML\\TH6_1.xml";
        protected void Page_Load(object sender, EventArgs e)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaLop", typeof(string));
            foreach(XElement x in xmlDoc1.Descendants("QLSV").Elements("MaLop"))
            {
                DataRow row;
                row = dt.NewRow();
                row["MaLop"] = x.Attribute("id").Value;
                dt.Rows.Add(row);
            }
            if (!IsPostBack)
            {
                cboMalop.DataSource = dt;
                cboMalop.DataTextField = "MaLop";
                cboMalop.DataBind();
            }       
        }

        protected void btnThemLop_Click(object sender, EventArgs e)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            xmlDoc1.Element("QLSV").Add(
                                        new XElement("MaLop",
                                        new XAttribute("id", txtLop.Text)));
            xmlDoc1.Save("D:\\XML\\TH6_1.xml");
        }

        protected void btnThemSV_Click(object sender, EventArgs e)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            foreach(XElement x in xmlDoc1.Descendants("QLSV").Elements("MaLop"))
            {
                if (x.Attribute("id").Value.ToString() == cboMalop.Items[cboMalop.SelectedIndex].Value)
                {
                    x.Add(
                            new XElement("SV",
                            new XElement("Masv",txtMasv.Text),
                            new XElement("Tensv",txtHoten.Text),
                            new XElement("Gioitinh",txtGioitinh.Text),
                            new XElement("DTB",txtDiemTB.Text)));
                }
            }
            xmlDoc1.Save("D:\\XML\\TH6_1.xml");
        }
    }
}