﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Data;

namespace DanhSach
{
    public partial class Web_SinhVien : System.Web.UI.Page
    {
        string path = "D:\\XML\\TH6_1.xml";
        public void LoadData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("MaLop", typeof(string));
            dt.Columns.Add("Masv", typeof(string));
            dt.Columns.Add("Tensv", typeof(string));
            dt.Columns.Add("Gioitinh", typeof(string));
            dt.Columns.Add("DTB", typeof(string));
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList danhsach = xmlDoc.GetElementsByTagName("MaLop");

            for(int i = 0; i < danhsach.Count; i++)
            {
                string malop = danhsach[i].Attributes[0].InnerText;
                for(int j = 0; j< danhsach[i].ChildNodes.Count; j++)
                {
                    string masv = danhsach[i].ChildNodes[j].ChildNodes[0].InnerText;
                    string ten = danhsach[i].ChildNodes[j].ChildNodes[1].InnerText;
                    string gt = danhsach[i].ChildNodes[j].ChildNodes[2].InnerText;
                    string dtb = danhsach[i].ChildNodes[j].ChildNodes[3].InnerText;
                    DataRow row;
                    row = dt.NewRow();
                    row["MaLop"] = malop;
                    row["Masv"] = masv;
                    row["Tensv"] = ten;
                    row["Gioitinh"] = gt;
                    row["DTB"] = dtb;
                    dt.Rows.Add(row);
                }
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadData();
            }
        }

        protected void btnDTB_Click(object sender, EventArgs e)
        {
            string xpath = "//SV[DTB>5]";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList node = xmlDoc.SelectNodes(xpath);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaLop", typeof(string));
            dt.Columns.Add("Masv", typeof(string));
            dt.Columns.Add("Tensv", typeof(string));
            dt.Columns.Add("Gioitinh", typeof(string));
            dt.Columns.Add("DTB", typeof(string));
            GridView1.Columns[0].Visible = false;// an cot ma lop
            foreach(XmlNode n in node)
            {
                string masv = n.ChildNodes[0].InnerText;
                string tensv = n.ChildNodes[1].InnerText;
                string gt = n.ChildNodes[2].InnerText;
                string dtb = n.ChildNodes[3].InnerText;
                DataRow row;
                row = dt.NewRow();
                row["MaLop"] = "";
                row["Masv"] = masv;
                row["Tensv"] = tensv;
                row["Gioitinh"] = gt; 
                row["DTB"] =dtb;
                dt.Rows.Add(row);
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }

        protected void btnLoc_Click(object sender, EventArgs e)
        {
            string xpath = "//MaLop[@id='" + ma.Text + "']";
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList danhsach = xmlDoc.SelectNodes(xpath);
            DataTable dt = new DataTable();
            dt.Columns.Add("MaLop", typeof(string));
            dt.Columns.Add("Masv", typeof(string));
            dt.Columns.Add("Tensv", typeof(string));
            dt.Columns.Add("Gioitinh", typeof(string));
            dt.Columns.Add("DTB", typeof(string));
            for(int i=0; i < danhsach.Count; i++)
            {
                string malop = danhsach[i].Attributes[0].InnerText;
                for (int j = 0; j < danhsach[i].ChildNodes.Count; j++)
                {
                    string masv = danhsach[i].ChildNodes[j].ChildNodes[0].InnerText;
                    string ten = danhsach[i].ChildNodes[j].ChildNodes[1].InnerText;
                    string gt = danhsach[i].ChildNodes[j].ChildNodes[2].InnerText;
                    string dtb = danhsach[i].ChildNodes[j].ChildNodes[3].InnerText;
                    DataRow row;
                    row = dt.NewRow();
                    row["MaLop"] = malop;
                    row["Masv"] = masv;
                    row["Tensv"] = ten;
                    row["Gioitinh"] = gt;
                    row["DTB"] = dtb;
                    dt.Rows.Add(row);
                }
            }
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
    }
}