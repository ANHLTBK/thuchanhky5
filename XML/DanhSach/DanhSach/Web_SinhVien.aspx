﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Web_SinhVien.aspx.cs" Inherits="DanhSach.Web_SinhVien" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: center;
        }
        .auto-style3 {
            width: 421px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tbody class="auto-style2">
                    <tr>
                        <td>DANH SÁCH SINH VIÊN</td>
                    </tr>
                    <tr>
                        <td>
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <table class="auto-style1">
                                            <tr>
                                                <td class="auto-style3">
                                                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="MaLop">
                                                        <Columns>
                                                            <asp:BoundField DataField="MaLop" HeaderText="Lớp" />
                                                            <asp:BoundField DataField="Masv" HeaderText="Mã Sinh Viên" />
                                                            <asp:BoundField DataField="Tensv" HeaderText="Tên Sinh Viên" />
                                                            <asp:BoundField DataField="Gioitinh" HeaderText="Giới Tính" />
                                                            <asp:BoundField DataField="DTB" HeaderText="Điểm TB" />
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                                <td>
                                                    <table class="auto-style1">
                                                        <tr>
                                                            <td>
                                                                <asp:Button ID="btnDTB" runat="server" OnClick="btnDTB_Click" Text="Sinh viên có ĐTB &gt; 5" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Nhập Mã lớp
                                                                <asp:TextBox ID="ma" runat="server"></asp:TextBox>
                                                                <asp:Button ID="btnLoc" runat="server" OnClick="btnLoc_Click" Text="Lọc" Width="72px" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
