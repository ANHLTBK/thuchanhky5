﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TheDanhSach.aspx.cs" Inherits="DanhSach.TheDanhSach" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            text-align: justify;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table class="auto-style1">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <table class="auto-style1">
                            <tr>
                                <td>THÊM LỚP</td>
                                <td class="auto-style2">THÊM SINH VIÊN</td>
                            </tr>
                            <tr>
                                <td>Lớp<asp:TextBox ID="txtLop" runat="server"></asp:TextBox>
                                </td>
                                <td class="auto-style2">Mã Sinh Viên:&nbsp;
                                    <asp:TextBox ID="txtMasv" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnThemLop" runat="server" OnClick="btnThemLop_Click" Text="Thêm" />
                                </td>
                                <td class="auto-style2">Họ Tên: <asp:TextBox ID="txtHoten" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="auto-style2">Giới Tính:
                                    <asp:TextBox ID="txtGioitinh" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="auto-style2">Điểm TB:
                                    <asp:TextBox ID="txtDiemTB" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="auto-style2">Chọ lớp:
                                    <asp:DropDownList ID="cboMalop" runat="server">
                                    </asp:DropDownList>
&nbsp;<asp:Button ID="btnThemSV" runat="server" OnClick="btnThemSV_Click" Text="Thêm" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
