﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace NHANSU
{
    public partial class Nhanvien : System.Web.UI.Page
    {
        string path = @"G:\Documents\NHANSU\NHANSU\Nhansu.xml";

        public void LoadData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Phongban", typeof(string));
            dt.Columns.Add("Manv", typeof(string));
            dt.Columns.Add("Tennv", typeof(string));
            dt.Columns.Add("Luong", typeof(string));

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList danhsach = xmlDoc.GetElementsByTagName("Phongban");

            for (int i = 0; i < danhsach.Count; i++)
            {
                string Phongban = danhsach[i].Attributes[0].InnerText;
                for (int j = 0; j < danhsach[i].ChildNodes.Count; j++)
                {
                    string masv = danhsach[i].ChildNodes[j].Attributes[0].InnerText;
                    string ten = danhsach[i].ChildNodes[j].Attributes[1].InnerText;
                    string luong = danhsach[i].ChildNodes[j].Attributes[2].InnerText;

                    DataRow row;
                    row = dt.NewRow();
                    row["Phongban"] = Phongban;
                    row["Manv"] = masv;
                    row["Tennv"] = ten;
                    row["Luong"] = luong;
                    dt.Rows.Add(row);
                }
            }
            GridVNV.DataSource = dt;
            GridVNV.DataBind();

        }

        protected void Page_Load(object sender, EventArgs e)
        {
            XDocument docNV = XDocument.Load(path);
            DataTable dt1 = new DataTable();
            dt1.Columns.Add("Phongban", typeof(string));
            foreach (XElement x in docNV.Descendants("qlns").Elements("Phongban"))
            {
                DataRow row = dt1.NewRow();
                row["Phongban"] = x.Attribute("Tenphong").Value;
                dt1.Rows.Add(row);
            }

            if (!IsPostBack)
            {
                LoadData();

                cmbPhongBan.DataSource = dt1;
                cmbPhongBan.DataTextField = "Phongban";
                cmbPhongBan.DataBind();
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);

            string xpath = "//Nhanvien[@Luong > 5000000 and @Luong < 8000000]"; // la attribute
            XmlNodeList note = xmlDoc.SelectNodes(xpath);


            DataTable dt = new DataTable();
            dt.Columns.Add("Phongban", typeof(string));
            dt.Columns.Add("Manv", typeof(string));
            dt.Columns.Add("Tennv", typeof(string));
            dt.Columns.Add("Luong", typeof(string));

            for (int i = 0; i < note.Count; i++)
            {
                string manv = note[i].Attributes[0].InnerText;
                string ten = note[i].Attributes[1].InnerText;
                string luong = note[i].Attributes[2].InnerText;
                DataRow row;
                row = dt.NewRow();
                row["Phongban"] = "";
                row["Manv"] = manv;
                row["Tennv"] = ten;
                row["Luong"] = luong;
                dt.Rows.Add(row);
            }

            

            GridVNV.DataSource = dt;
            GridVNV.DataBind();
        }

        protected void Button2_Click(object sender, EventArgs e)
        {

            string xpath = "//Phongban[@Tenphong= '" + txtPhongban.Text + "']";

            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(path);
            XmlNodeList note = xmlDoc.SelectNodes(xpath);

            DataTable dt = new DataTable();
            dt.Columns.Add("Phongban", typeof(string));
            dt.Columns.Add("Manv", typeof(string));
            dt.Columns.Add("Tennv", typeof(string));
            dt.Columns.Add("Luong", typeof(string));

            for (int i = 0; i < note.Count; i++)
            {
                string Phongban = note[i].Attributes[0].InnerText;
                for (int j = 0; j < note[i].ChildNodes.Count; j++)
                {
                    string masv = note[i].ChildNodes[j].Attributes[0].InnerText;
                    string ten = note[i].ChildNodes[j].Attributes[1].InnerText;
                    string luong = note[i].ChildNodes[j].Attributes[2].InnerText;

                    DataRow row;
                    row = dt.NewRow();
                    row["Phongban"] = Phongban;
                    row["Manv"] = masv;
                    row["Tennv"] = ten;
                    row["Luong"] = luong;
                    dt.Rows.Add(row);
                }
            }


            GridVNV.DataSource = dt;
            GridVNV.DataBind();

        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            XDocument xmlDoc1 = XDocument.Load(path);
            var root = xmlDoc1.Descendants("qlns").Elements("Phongban").Where(x => x.Attribute("Tenphong").Value == cmbPhongBan.Text).FirstOrDefault();


            root.Add(new XElement("Nhanvien",
                                            new XAttribute("Masv", txtMaNV.Text),
                                            new XAttribute("Hoten", txtTenNV.Text),
                                            new XAttribute("Gioitinh", txtLuong.Text)));
            xmlDoc1.Save(path);
            LoadData();
        }
    }
    }