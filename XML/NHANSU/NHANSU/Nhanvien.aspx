﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Nhanvien.aspx.cs" Inherits="NHANSU.Nhanvien" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
            height: 257px;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            width: 409px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style3" rowspan="5">
                    <asp:GridView ID="GridVNV" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal">
                        <Columns>
                            <asp:BoundField DataField="Phongban" HeaderText="Phòng ban" />
                            <asp:BoundField DataField="Manv" HeaderText="Mã NV" />
                            <asp:BoundField DataField="Tennv" HeaderText="Tên NV" />
                            <asp:BoundField DataField="Luong" HeaderText="Luong" />
                            <asp:ButtonField CommandName="Edit" Text="Edit" />
                        </Columns>
                        <FooterStyle BackColor="White" ForeColor="#333333" />
                        <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="White" ForeColor="#333333" />
                        <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F7F7F7" />
                        <SortedAscendingHeaderStyle BackColor="#487575" />
                        <SortedDescendingCellStyle BackColor="#E5E5E5" />
                        <SortedDescendingHeaderStyle BackColor="#275353" />
                    </asp:GridView>
                </td>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Loc NV" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:TextBox ID="txtPhongban" runat="server"></asp:TextBox>
&nbsp;&nbsp;
                    <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Loc Phong ban" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Phong ban:
                    <asp:DropDownList ID="cmbPhongBan" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>Ma NV
                    <asp:TextBox ID="txtMaNV" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>Ten NV
                    <asp:TextBox ID="txtTenNV" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp; Luong
                    <asp:TextBox ID="txtLuong" runat="server"></asp:TextBox>
&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Insert" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
