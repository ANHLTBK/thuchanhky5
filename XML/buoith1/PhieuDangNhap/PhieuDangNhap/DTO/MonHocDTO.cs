﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhieuDangNhap.DTO
{
    public class MonHocDTO
    {
        private string MaMon;
        private string TenMon;
        private int SoTC;


        public string MAMON
        {
            get { return MaMon; }
            set { MaMon = value; }
        }
        public string TENMON
        {
            get { return TenMon; }
            set { TenMon = value; }
        }
        public int SOTC
        {
            get { return SoTC; }
            set { SoTC = value; }
        }
    }
}
