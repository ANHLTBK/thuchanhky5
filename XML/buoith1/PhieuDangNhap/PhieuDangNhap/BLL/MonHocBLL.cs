﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PhieuDangNhap.DTO;
using System.Windows.Forms;
using System.Xml;

namespace PhieuDangNhap.BLL
{
    public class MonHocBLL
    {
        XmlDocument doc = new XmlDocument();
        XmlElement root;
        string fileName = @"G:\Documents\thuchanhky5\XML\fileXML.xml";
        public MonHocBLL()
        {
            doc.Load(fileName);
            root = doc.DocumentElement;
        }
        // them
        public void Them(MonHocDTO sachThem)
        {
            //tạo nút sách
            XmlNode sach = doc.CreateElement("MH");

            //tạo nút con của sách là masach
            XmlElement masach = doc.CreateElement("MaMon");
            masach.InnerText = sachThem.MAMON;//gán giá trị cho mã sách
            sach.AppendChild(masach);//thêm masach vào trong sách(sach nhận masach là con)

            XmlElement tensach = doc.CreateElement("TenMon");
            tensach.InnerText = sachThem.TENMON;
            sach.AppendChild(tensach);

            XmlElement soluong = doc.CreateElement("SoTinChi");
            soluong.InnerText = sachThem.SOTC.ToString();
            sach.AppendChild(soluong);
            root.AppendChild(sach);
            doc.Save(fileName);

        }
    }
   
}
