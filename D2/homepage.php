<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Trang Chủ</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	</head>
	<body>
		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
		<div class="m-auto" style="padding-top: 15px; width: 800px">
			<form method="POST">
				<?php
					$error = array();
        			$data = array();
        			if (!empty($_POST['ok'])){
            			// Lấy dữ liệu
			            $data['fullname'] = isset($_POST['fullname']) ? $_POST['fullname'] : '';
			            $data['email'] = isset($_POST['email']) ? $_POST['email'] : '';
			            $data['phone'] = isset($_POST['phone']) ? $_POST['phone'] : '';
			            $data['otherphone'] = isset($_POST['otherphone']) ? $_POST['otherphone'] : '';
			 
			            // Kiểm tra định dạng dữ liệu
			            require('./validate.php');
			            if (empty($data['fullname'])){
			                $error['fullname'] = 'Bạn chưa nhập tên';
			            } else if (!is_name($data['fullname'])) {
			            	$error['fullname'] = 'Tên không đúng định dạng';
			            }
			 
			            if (empty($data['email'])){
			                $error['email'] = 'Bạn chưa nhập email';
			            }
			            else if (!is_email($data['email'])){
			                $error['email'] = 'Email không đúng định dạng';
			            }
			 
			            if (empty($data['phone'])){
			                $error['phone'] = 'Bạn chưa nhập số điện thoại';
			            }
			            else if (!is_phone($data['phone'])) {
			            	 $error['phone'] = 'Định dạng điện thoại chưa đúng';
			            }
			            if (empty($data['otherphone'])){
			                $error['otherphone'] = 'Bạn chưa nhập số điện thoại';
			            }
			            else if (!is_phone($data['otherphone'])) {
			            	 $error['otherphone'] = 'Định dạng điện thoại chưa đúng';
			            }
			            if ($data['phone'] != $data['otherphone']) {
			            	$error['otherphone'] = 'Nhập lại chưa đúng';
			            }

			 			if ($data) {
			 				# code...
			 			}
			            // Lưu dữ liệu
			            if (!$error){
			            	include('connect.php');
			            	$gender = $_POST['gender'];
			            	$fullname = $_POST['fullname'];
			            	$city = $_POST['city'];
			            	$address = $_POST['txtdiachi'];
			            	$phone = $_POST['phone'];
			            	$otherphone = $_POST['otherphone'];
			            	$email = $_POST['email'];
			            	if(isset($_POST['cbox'])&&$_POST['cbox']=="yes"){
						   	$checbok='1';}
							else{
						  	 $checbok='0';
						  	}
						  	$yeucau = $_POST['txtyeucau'];
			            	$sql = "Insert into inp (hoten, gender, city, diachi, sodt, sodtkhac, yeucau, hoadon, email) values('$fullname','$gender','$city','$address','$phone','$otherphone','$yeucau','$checbok','$email')";
							$thucthi = mysqli_query($conn,$sql);
							if ($thucthi) {
							echo "<script>alert('Thành công')</script>";
							}
			            }
			            else{
			                echo "<script>alert('Thông tin sai hoặc thiếu - Vui lòng nhập lại')</script>";
			            }
        		}
				?>
				<table class="form-control" align="center" cellpadding="5">
					<tr>
						<td colspan="4">
							<h1>Thông tin liên hệ</h1>
							<p>Mọi trường có dấu * đều phải điền không được bỏ trống</p>
						</td>
					</tr>
					<tr>
						<td>
							<label>Quý Danh</label>
							<select class="form-control" name="gender">
								<option value="">--Chọn--</option>
								<option value="Male">Male</option>
								<option value="Female">Female</option>
							</select>
						</td>
						<td>
							<label>Họ và tên:</label>
							<input type="text" name="fullname" class="form-control" value="<?php echo isset($data['fullname']) ? $data['fullname'] : ''; ?>"><?php echo isset($error['fullname']) ? $error['fullname'] : ''; ?>
						</td>
						<td>
						<label>Thành phố: </label>
							<select class="form-control" name="city">
							<option value="">--Chọn--</option>
							<option value="Hà Nội">Hà Nội</option>
							<option value="Hồ Chí Minh">Hồ Chí Minh</option>
						</td>
						<td>
							<label>Địa chỉ:</label>
							<input type="text" name="txtdiachi" class="form-control">
						</td>
					</tr>
					<tr>
						<td>
							<label>Số điện thoại:</label>
							<input type="number" name="phone" class="form-control" value="<?php echo isset($data['phone']) ? $data['phone'] : ''; ?>"><?php echo isset($error['phone']) ? $error['phone'] : ''; ?>
						</td>
						<td>
							<label>Số điện thoại khác:</label>
							<input type="number" name="otherphone" class="form-control" value="<?php echo isset($data['otherphone']) ? $data['otherphone'] : ''; ?>"><?php echo isset($error['otherphone']) ? $error['otherphone'] : ''; ?>
						</td>
						<td colspan="2" rowspan="2">
							<label>Yêu cầu:</label>
							<textarea rows="5" cols="20" class="form-control" name="txtyeucau"></textarea>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<label>Email:</label>
							<input class="form-control" type="text" name="email" id="email" value="<?php echo isset($data['email']) ? $data['email'] : ''; ?>"/>
                        <?php echo isset($error['email']) ? $error['email'] : ''; ?>
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="checkbox" name="cbox" value="yes"> Tôi muốn xuất hóa đơn đỏ
						</td>
					</tr>
					<tr>
						<td colspan="4" align="center">
						<input type="submit" name="ok" value="Gửi Yêu Cầu" class="btn-primary"> 
						</td>
					</tr>
				</table>
			</form>
		</div>
	</body>
</html>